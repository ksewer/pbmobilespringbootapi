package com.example.pbmobilespringbootapi;

import java.util.ArrayList;
import java.util.List;

public class ErrorMessage {

    private List<OneMessage> messages = new ArrayList<>();

    public void addMessage(OneMessage message) {
        messages.add(message);
    }

    public List<OneMessage> getMessages() {
        return messages;
    }

    public static class OneMessage {
        String errorNumber;
        String errorMessage;

        public OneMessage(String errorNumber, String errorMessage) {
            this.errorNumber = errorNumber;
            this.errorMessage = errorMessage;
        }

        public String getErrorNumber() {
            return errorNumber;
        }

        public void setErrorNumber(String errorNumber) {
            this.errorNumber = errorNumber;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public String toString() {
            return "OneMessage{" +
                    "errorNumber='" + errorNumber + '\'' +
                    ", errorMessage='" + errorMessage + '\'' +
                    '}';
        }
    }

    public String getMessageByNumber(String element) {
        for (OneMessage m : messages) {
            if (m.getErrorNumber().equals(element)) {
                return m.getErrorMessage();
            }
        }
        return null;
    }

    public static ErrorMessage generateNotFoundMessage(String objectName, String id) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.getMessages().clear();
        errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorNumberConst.NOTFOUNDED.toString(), objectName + " with id: " + id + " not found!"));
        return errorMessage;
    }

    public enum ErrorNumberConst {
        EXISTOTHER("4001"),
        NULLARGUMENT("4002"),
        RESERVATIONINPAST("4003"),
        NOTFOUNDED("4004"),
        OTHERRESERVATIONTOTHISTERM("4005"),
        LOGINFAILED("4006"),
        NOTENOUGHPOINTS("4007")
        ;

        private String number;

        ErrorNumberConst(String number) {
          this.number = number;
        }

        @Override
        public String toString() {
            return number;
        }
    }
}
