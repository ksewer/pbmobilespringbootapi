package com.example.pbmobilespringbootapi;

import org.springframework.stereotype.Service;

@Service
public class Worker {
    String name;
    String function;

    public Worker(String name, String function) {
        this.name = name;
        this.function = function;
    }

    public Worker() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }
}
