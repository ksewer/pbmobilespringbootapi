package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.Manager.CompanyManager;
import com.example.pbmobilespringbootapi.Manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/company")
public class CompanyAPI {
    public CompanyManager companyManager;
    public UserManager userManager;

    @Autowired
    public CompanyAPI(CompanyManager companyManager, UserManager userManager) {
        this.companyManager = companyManager;
        this.userManager = userManager;
    }

    @GetMapping("/getAll")
    public Iterable<Company> getAllCompany(){
        return companyManager.getAllCompany();
    }

    @PostMapping("/addCompany/{idUser}")
    public void addCompany(@RequestBody Company company, @PathVariable Long idUser){
        Object companyObj = companyManager.addCompany(company);
        if(companyObj instanceof Company){
            userManager.setCompanyToUser(idUser,company);
        }
    }

    @PutMapping("/updateCompany/{id}")
    public Object updateCompany(@RequestBody Company company, @PathVariable Long id){
        return companyManager.updateCompanyData(company, id);
    }

    @GetMapping("/getCompanyByUser/{id}")
    public Object getCompanyByUser(@PathVariable Long id) {
        return userManager.getCompanyByUser(id);
    }

    @DeleteMapping("/deleteCompany/{id}")
    public Object deleteCompany(@PathVariable Long id){
        return companyManager.deleteCompany(id);
    }

    @GetMapping("/getCompanyById/{id}")
    public Object getCompanyById(@PathVariable Long id){
        return companyManager.getCompanyById(id);
    }

    @GetMapping("/getCompanyByNIP/{nip}")
    public Object getCompanyByNIP(@PathVariable String nip){
        return companyManager.getCompanyByNip(nip);
    }

    @GetMapping("/getCompanyByRegon/{regon}")
    public Object getCompanyByRegon(@PathVariable String regon){
        return companyManager.getCompanyByRegon(regon);
    }
}
