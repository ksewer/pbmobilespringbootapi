package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Receipt;
import com.example.pbmobilespringbootapi.Manager.ReceiptManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/receipt")
public class ReceiptAPI {

    private ReceiptManager receiptManager;

    @Autowired
    public ReceiptAPI(ReceiptManager receiptManager) {
        this.receiptManager = receiptManager;
    }

    @PostMapping("/saveReceipt")
    public void saveReceipt(@RequestBody Receipt receipt){
        receiptManager.saveReceipt(receipt);
    }

    @GetMapping("/getAll")
    public Iterable<Receipt> getAllReceipt(){
        return receiptManager.getAllReceipt();
    }

    @PutMapping("/updatePaymentMethod/{id}")
    public Object updatePaymentMethod(@PathVariable Long id, @RequestBody Receipt receipt){
        return receiptManager.updatePaymentMethod(id, receipt);
    }

}
