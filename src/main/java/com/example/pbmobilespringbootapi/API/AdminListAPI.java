package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Admin;
import com.example.pbmobilespringbootapi.Manager.AdminListManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/adminList")
public class AdminListAPI {

    private AdminListManager adminListManager;

    @Autowired
    public AdminListAPI(AdminListManager adminListManager) {
        this.adminListManager = adminListManager;
    }

    @PostMapping("/addAdmin")
    public Object addAdmin(@RequestBody Admin admin){
        return adminListManager.addAdmin(admin);
    }

    @GetMapping("/checkEmail")
    public Object checkEmail(@RequestBody Admin admin){
        return adminListManager.checkEmail(admin);
    }

    @DeleteMapping("/deleteAdmin/{id}")
    public Object deleteAdmin(@PathVariable Long id){
        return adminListManager.deleteAdmin(id);
    }

    @PutMapping("/updateProductPrice/{idProduct}")
    public void updateProductPrice(@PathVariable Long idProduct, @RequestParam Float price){
        adminListManager.updateProductPrice(idProduct,price);
    }
}
