package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.PBLoyaltyProduct;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.Manager.PBLoyaltyProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loyaltyProduct")
public class PBLoyaltyProductAPI {

    PBLoyaltyProductManager pbLoyaltyProductManager;

    @Autowired
    public PBLoyaltyProductAPI(PBLoyaltyProductManager pbLoyaltyProductManager) {
        this.pbLoyaltyProductManager = pbLoyaltyProductManager;
    }

    @GetMapping("/getAll")
    public Iterable<PBLoyaltyProduct> getAllLoyaltyProduct(){
        return pbLoyaltyProductManager.getAllLoyaltyProduct();
    }

    @PostMapping("/addLoyaltyProduct/{idProduct}/{points}")
    public void addLoyaltyProduct(@PathVariable Integer points, @PathVariable Long idProduct){
        pbLoyaltyProductManager.addLoyaltyProduct(points, idProduct);
    }

    @PutMapping("/updateLoyaltyProduct/{idProduct}/{points}")
    public void updateLoyaltyProduct(@PathVariable Integer points, @PathVariable Long idProduct){
        pbLoyaltyProductManager.updateLoylatyProduct(points,idProduct);
    }

    @DeleteMapping("/deleteLoyaltyProduct/{id}")
    public Object deleteLoylatyProduct(@PathVariable Long id){
        return pbLoyaltyProductManager.deleteLoyaltyProduct(id);
    }

    @GetMapping("/getLoyaltyProductById/{id}")
    public PBLoyaltyProduct getLoyaltyProductById(@PathVariable Long id) {
        return pbLoyaltyProductManager.getLoylatyProductById(id);
    }

    @PutMapping("/changePointsToLoyaltyProduct/{idUser}/{idLoyaltyProduct}")
    public Object changePointsToLoyaltyProduct(@PathVariable Long idUser, @PathVariable Long idLoyaltyProduct){
        return pbLoyaltyProductManager.changePointsToProduct(idUser, idLoyaltyProduct);
    }

}
