package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.RefuelingHistory;
import com.example.pbmobilespringbootapi.Manager.RefuelingHistoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/refuelingHistory")
public class RefuelingHistoryAPI {

    private RefuelingHistoryManager refuelingHistoryManager;

    @Autowired
    public RefuelingHistoryAPI(RefuelingHistoryManager refuelingHistoryManager) {
        this.refuelingHistoryManager = refuelingHistoryManager;
    }

    @GetMapping("/getAll")
    public Iterable<RefuelingHistory> getAll(){
        return refuelingHistoryManager.getAll();
    }

    @GetMapping("/getAllByUser/{idUser}")
    public Iterable<RefuelingHistory> getAllByUser(@PathVariable Long idUser){
        return refuelingHistoryManager.getAllByUser(idUser);
    }

    @PostMapping("/add")
    public Object add(@RequestBody RefuelingHistory refuelingHistory){
        return refuelingHistoryManager.add(refuelingHistory);
    }
}
