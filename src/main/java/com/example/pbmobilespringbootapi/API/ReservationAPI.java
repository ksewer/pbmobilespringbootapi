package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Reservation;
import com.example.pbmobilespringbootapi.Manager.ReservationManager;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/reservation")
public class ReservationAPI {

    private ReservationManager reservationManager;

    @Autowired
    public ReservationAPI(ReservationManager reservationManager) {
        this.reservationManager = reservationManager;
    }

    @PostMapping("/add/{idUser}")
    public void addReservation(@RequestBody Reservation reservation, @PathVariable Long idUser) {
        reservationManager.addReservation(reservation, idUser);
    }

    @GetMapping("/getAll")
    public Iterable<Reservation> getAll() {
        return reservationManager.getAll(false, -1L);
    }

    @GetMapping("/getAllByUser/{id}")
    public Iterable<Reservation> getAllByUser(@PathVariable Long id) {
        return reservationManager.getAll(true, id);
    }

    @GetMapping("/getAllActive")
    public Iterable<Reservation> getAllActive() {
        return reservationManager.getAllActive(false, -1L);
    }

    @GetMapping("/getAllActiveByUser/{id}")
    public Iterable<Reservation> getAllActiveByUser(@PathVariable Long id) {
        return reservationManager.getAllActive(true, id);
    }

    @GetMapping("/getHistoryByUser/{id}")
    public Iterable<Reservation> getHistoryByUser(@PathVariable Long id) {
        return reservationManager.getHistoryByUser(id);
    }

    @GetMapping("/getAvailableTerms/{date}")
    public List<ObjectNode> getAvailableTerm(@PathVariable String date) {
        List<ObjectNode> availableTermObjectNode = reservationManager.getAvailableTermObjectNode(date);
        if (availableTermObjectNode == null)
            return new ArrayList<>();
        else
            return availableTermObjectNode;
    }

    @GetMapping("/getImmediateAvailableTerm")
    public ObjectNode getImmediateAvailableTerm() {
        return reservationManager.getImmediateAvailableTerm();
    }

    @DeleteMapping("/deleteReservation/{id}")
    public Object deleteReservation(@PathVariable Long id) {
        return reservationManager.deleteReservation(id);
    }
}
