package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.Manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestAPI {

    private AdminListManager adminListManager;
    private InvoiceManager invoiceManager;
    private ReceiptManager receiptManager;
    private MagazinePositionManager magazinePositionManager;
    private PBLoyaltyProductManager pbLoyaltyProductManager;
    private RefuelingHistoryManager refuelingHistoryManager;
    private ReservationManager reservationManager;
    private UserManager userManager;
    private ProductManager productManager;
    private InfoAppManager infoAppManager;

    @Autowired
    public TestAPI(AdminListManager adminListManager, InvoiceManager invoiceManager, ReceiptManager receiptManager, MagazinePositionManager magazinePositionManager, PBLoyaltyProductManager pbLoyaltyProductManager, RefuelingHistoryManager refuelingHistoryManager, ReservationManager reservationManager, UserManager userManager, ProductManager productManager, InfoAppManager infoAppManager) {
        this.adminListManager = adminListManager;
        this.invoiceManager = invoiceManager;
        this.receiptManager = receiptManager;
        this.magazinePositionManager = magazinePositionManager;
        this.pbLoyaltyProductManager = pbLoyaltyProductManager;
        this.refuelingHistoryManager = refuelingHistoryManager;
        this.reservationManager = reservationManager;
        this.userManager = userManager;
        this.productManager = productManager;
        this.infoAppManager = infoAppManager;
    }

    @GetMapping("/fellDb")
    public void fellDb() {
        infoAppManager.fellDb();
        userManager.fellDb();
        productManager.fellDb();
        magazinePositionManager.fellDb();
        adminListManager.fellDb();
        invoiceManager.fellDb();
        receiptManager.fellDb();
        pbLoyaltyProductManager.fellDb();
        refuelingHistoryManager.fellDb();
        reservationManager.fellDb();
    }
}
