package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.Manager.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductAPI {

    ProductManager productManager;

    @Autowired
    public ProductAPI(ProductManager productManager) {
        this.productManager = productManager;
    }

    @GetMapping("/getAll")
    public Iterable<Product> getAllProducts() {
        return productManager.getAllProducts();
    }

    @GetMapping("/getProductById/{id}")
    public Product getProductById(@PathVariable Long id) {
        return productManager.getProductById(id);
    }

    @GetMapping("/getProductsByCategory/{category}")
    public List<Product> getProductsByCategory(@PathVariable String category) {
        return productManager.getProductsByCategory(category);
    }

    @PostMapping("/addProduct")
    public Object addProduct(@RequestBody Product product) {
        return productManager.addProduct(product);
    }

    @DeleteMapping("/deleteProduct/{id}")
    public Object deleteProduct(@PathVariable Long id) {
        return productManager.deleteProduct(id);
    }

    @PutMapping("/updateProduct/{id}")
    public void updateProduct(@PathVariable Long id, @RequestBody Product product) {
        productManager.updateProduct(id, product);
    }
}
