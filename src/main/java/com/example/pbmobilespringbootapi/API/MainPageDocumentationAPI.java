package com.example.pbmobilespringbootapi.API;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainPageDocumentationAPI {

    @GetMapping("/")
    public String showDocumentation(){
        return "documentation";
    }

}
