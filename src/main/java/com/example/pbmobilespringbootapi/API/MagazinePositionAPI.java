package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.MagazinePosition;
import com.example.pbmobilespringbootapi.Manager.MagazinePositionManager;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/magazinePosition")
public class MagazinePositionAPI {

    private MagazinePositionManager magazinePositionManager;

    @Autowired
    public MagazinePositionAPI(MagazinePositionManager magazinePositionManager) {
        this.magazinePositionManager = magazinePositionManager;
    }

    @PostMapping("/addMagazinePosition")
    public Object addMagazinePosition(@RequestBody MagazinePosition magazinePosition) {
        return magazinePositionManager.addMagazinePosition(magazinePosition);
    }

    @PutMapping("/updateQuantity/{id}")
    public Object updateQuantity(@PathVariable Long id, @RequestBody MagazinePosition magazinePosition) {
        return magazinePositionManager.updateQuantity(id, magazinePosition.getQuantity());
    }

    @DeleteMapping("/deleteMagazinePosition/{id}")
    public Object deleteMagazinePosition(@PathVariable Long id) {
        return magazinePositionManager.deleteMagazineProduct(id);
    }

    @GetMapping("/getAll")
    public Iterable<MagazinePosition> getAll() {
        return magazinePositionManager.getAll();
    }

    @GetMapping("/getQuantityByProductId/{id}")
    public Object getQuantityByProductId(@PathVariable Long id) {
        return magazinePositionManager.getQuantityByProductID(id);
    }

    @PutMapping("/refuelToMaxLevel/{idProduct}")
    public void refuelToMaxLevel(@PathVariable Long idProduct){
        magazinePositionManager.refuelToMaxLevel(idProduct);
    }

    @GetMapping("/getMagazinePetrolStatus")
    public List<ObjectNode> getMagazinePetrolStatus(){
        return magazinePositionManager.getMagazinePetrolStatus();
    }
}
