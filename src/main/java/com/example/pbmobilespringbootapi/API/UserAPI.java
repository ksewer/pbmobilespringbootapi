package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.Manager.UserManager;
import com.example.pbmobilespringbootapi.UserMini;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserAPI {
    private UserManager userManager;

    @Autowired
    public UserAPI(UserManager userManager) {
        this.userManager = userManager;
    }

    @PostMapping("/register")
    public UserMini addUser(@RequestBody User user) {
        return userManager.addUserReturnUserMini(user);
    }

    @GetMapping("/getAll")
    public Iterable<User> getAllUsers() {
        return userManager.getAllUsers();
    }

    @GetMapping("/profile/{id}")
    public Object login(@PathVariable Long id) {
        return userManager.login(id);
    }

    @GetMapping("/{id}")
    public Object loginUserMini(@PathVariable Long id) {
        return userManager.loginUserMini(id);
    }

    @PutMapping("/updateUser/{id}")
    public Object updateUserData(@RequestBody User user, @PathVariable Long id) {
        return userManager.updateUserData(user, id);
    }

    @PutMapping("/addPoints/{id}")
    public void addPoints(@PathVariable Long id, @RequestBody User user){
        userManager.addPoints(id, user.getPoints());
    }

    @DeleteMapping("/deleteUser/{id}")
    public Object deleteUser(@PathVariable Long id) {
        return userManager.deleteUser(id);
    }
}
