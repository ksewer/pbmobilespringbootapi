package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.InfoApp;
import com.example.pbmobilespringbootapi.Manager.InfoAppManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/infoApp")
public class InfoAppAPI {

    private InfoAppManager infoAppManager;

    @Autowired
    public InfoAppAPI(InfoAppManager infoAppManager) {
        this.infoAppManager = infoAppManager;
    }

    @GetMapping("/getInfo")
    public Object getInfo(){
        return infoAppManager.getInfo();
    }

    @PutMapping("/updateInfo")
    public Object updateInfo(@RequestBody InfoApp infoApp){
        return infoAppManager.updateInfo(infoApp);
    }
}
