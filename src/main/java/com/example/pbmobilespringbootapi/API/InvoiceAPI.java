package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Invoice;
import com.example.pbmobilespringbootapi.Manager.InvoiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/invoice")
public class InvoiceAPI {

    private InvoiceManager invoiceManager;

    @Autowired
    public InvoiceAPI(InvoiceManager invoiceManager) {
        this.invoiceManager = invoiceManager;
    }

    @PostMapping("/saveInvoice")
    public void saveInvoice(@RequestBody Invoice invoice){
        invoiceManager.saveInvoice(invoice);
    }

    @GetMapping("/getAll")
    public Iterable<Invoice> getAllInvoice(){
        return invoiceManager.getAllInvoice();
    }

    @PutMapping("/updatePaymentMethod/{id}")
    public Object updatePaymentMethod(@PathVariable Long id, @RequestBody Invoice invoice){
        return invoiceManager.updatePaymentMethod(id, invoice);
    }

    @PutMapping("/updateCompanyOnInvoice/{id}")
    public Object updateCompanyOnInvoice(@PathVariable Long id, @RequestBody Invoice invoice){
        return invoiceManager.updateCompanyOnInvoice(id, invoice);
    }
}
