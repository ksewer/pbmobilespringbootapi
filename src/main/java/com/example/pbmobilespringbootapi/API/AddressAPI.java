package com.example.pbmobilespringbootapi.API;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.Manager.AddressManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
public class AddressAPI {

    private AddressManager addressManager;

    @Autowired
    public AddressAPI(AddressManager addressManager) {
        this.addressManager = addressManager;
    }

    @PostMapping("/addAddress")
    public Address addAddress(@RequestBody Address address) {
        return addressManager.addAddress(address);
    }

    @GetMapping("/getAll")
    public Iterable<Address> getAllAdress() {
        return addressManager.getAllAddress();
    }

    @PutMapping("/updateAddress/{id}")
    public Object updateAddress(@RequestBody Address address, @PathVariable Long id) {
        return addressManager.updateAddress(address, id);
    }

    @DeleteMapping("/deleteAddress/{id}")
    public Object deleteAddress(@PathVariable Long id){
        return addressManager.deleteAddress(id);
    }

    @GetMapping("/getAddress/{id}")
    public Object getAddressById(@PathVariable Long id){
        return addressManager.getAddressById(id);
    }

    @GetMapping("/wakeUp")
    public String wakeUp(){
        return "Wake up API";
    }
}
