package com.example.pbmobilespringbootapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PbmobilespringbootapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PbmobilespringbootapiApplication.class, args);
    }

}
