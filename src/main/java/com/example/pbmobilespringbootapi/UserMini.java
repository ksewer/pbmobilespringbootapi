package com.example.pbmobilespringbootapi;

public class UserMini {

    Long id;
    String nickname;
    Integer points;
    String userType;

    public UserMini(Long id, String nickname, Integer points, String userType) {
        this.id = id;
        this.nickname = nickname;
        this.points = points;
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
