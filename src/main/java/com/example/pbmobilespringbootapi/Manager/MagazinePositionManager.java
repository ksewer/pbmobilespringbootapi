package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.MagazinePosition;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.MagazinePositionRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MagazinePositionManager {

    private MagazinePositionRepository magazinePositionRepository;
    private ProductManager productManager;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    public MagazinePositionManager(MagazinePositionRepository magazinePositionRepository, ProductManager productManager) {
        this.magazinePositionRepository = magazinePositionRepository;
        this.productManager = productManager;
    }

    public Object addMagazinePosition(MagazinePosition magazinePosition) {
        if (magazinePosition.getProduct() == null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Product is null!"));
            return errorMessage;
        }
        Product productFinded = productManager.getProductById(magazinePosition.getProduct().getId());
        if (productFinded == null) {
            return ErrorMessage.generateNotFoundMessage("Product", magazinePosition.getProduct().getId().toString());
        }
        magazinePosition.setProduct(productFinded);

        MagazinePosition magazinePositionFinded = magazinePositionRepository.findFirstByProduct(productFinded);
        if (magazinePositionFinded != null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.EXISTOTHER.toString(), "MagazineProduct with this product already exists"));
            return errorMessage;
        } else {
            return magazinePositionRepository.save(magazinePosition);
        }
    }

    public Object updateQuantity(Long id, Float quantity) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return magazinePositionRepository.findById(id)
                .map(magazinePosition -> {
                    magazinePosition.setQuantity(quantity);
                    return magazinePositionRepository.save(magazinePosition);
                });
    }

    public Object deleteMagazineProduct(Long id) {
        Optional<MagazinePosition> magazinePosition = magazinePositionRepository.findById(id);
        if (magazinePosition.isPresent()) {
            magazinePositionRepository.delete(magazinePosition.get());
            return true;
        } else {
            ErrorMessage errorMessage = ErrorMessage.generateNotFoundMessage("MagazinePosition", id.toString());
            return errorMessage;
        }
    }

    public Iterable<MagazinePosition> getAll() {
        return magazinePositionRepository.findAll();
    }

    public Object getQuantityByProductID(Long id) {
        Product productFinded = productManager.getProductById(id);
        MagazinePosition magazinePositionFinded = magazinePositionRepository.findFirstByProduct(productFinded);
        if (magazinePositionFinded != null) {
            return magazinePositionFinded;
        } else {
            return ErrorMessage.generateNotFoundMessage("Product", id.toString());
        }
    }
    Float magazineMax = 10000f;
    public List<ObjectNode> getMagazinePetrolStatus(){
        List<Product> petrols = productManager.getProductsByCategory("Petrol");
        List<ObjectNode> objectNodeList = new ArrayList<>();
        for(Product product : petrols){
            MagazinePosition firstByProduct = magazinePositionRepository.findFirstByProduct(product);
            ObjectNode objectNode = mapper.createObjectNode();
            objectNode.put("idProduct", product.getId());
            objectNode.put("productName", product.getProductName());
            objectNode.put("productQuantityPercent", (firstByProduct.getQuantity()/magazineMax)*100);
            objectNodeList.add(objectNode);
                if(((firstByProduct.getQuantity()/magazineMax)*100)<5){
                refuelToMaxLevel(product.getId());
            }
        }
        return objectNodeList;
    }

    public void refuelToMaxLevel(Long productId){
        Product product = productManager.getProductById(productId);
        MagazinePosition firstByProduct = magazinePositionRepository.findFirstByProduct(product);
        if(firstByProduct != null){
            firstByProduct.setQuantity(magazineMax);
            magazinePositionRepository.save(firstByProduct);
        }
    }

    public void fellDb() {
        addMagazinePosition(new MagazinePosition(productManager.getProductById(1L), 2414.1f));
        addMagazinePosition(new MagazinePosition(productManager.getProductById(2L), 1323.2f));
        addMagazinePosition(new MagazinePosition(productManager.getProductById(3L), 3232.3f));
        addMagazinePosition(new MagazinePosition(productManager.getProductById(4L), 4141.4f));
    }

    private Object checkExist(Long id) {
        Optional<MagazinePosition> optional = magazinePositionRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("MagazineProduct", id.toString());
        } else
            return true;
    }
}
