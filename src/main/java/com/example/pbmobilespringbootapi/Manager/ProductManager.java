package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.ProductRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductManager {
    private ProductRepository productRepository;

    @Autowired
    public ProductManager(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public Iterable<Product> saveProducts(List<Product> productsOnReceipt) {
        return productRepository.saveAll(productsOnReceipt);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        return productOptional.orElse(null);
    }

    public List<Product> getProductsByCategory(String category) {
        return productRepository.findAllByCategory(category);
    }

    public Object addProduct(Product product) {
        if (product != null) {
            Product productFinded = productRepository.findFirstByProductNameAndCategory(product.getProductName(), product.getCategory());
            if (productFinded != null)
                return productFinded;
            else{
                product.setPriceNetto(product.getPriceBrutto()/1.23f);
                return productRepository.save(product);
            }
        }
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Argument is null!"));
        return errorMessage;
    }

    public Object deleteProduct(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            productRepository.delete(productOptional.get());
            return true;
        } else {
            ErrorMessage errorMessage = ErrorMessage.generateNotFoundMessage("Product", id.toString());
            return errorMessage;
        }
    }

    public void updateProduct(Long id, Product newProduct) {
        Optional<Product> productOptional = productRepository.findById(id);
        if (!productOptional.isPresent()) {
            ErrorMessage errorMessage = ErrorMessage.generateNotFoundMessage("Product", id.toString());
            return;//errorMessage;
        }
        productRepository.findById(id)
                .map(product -> {
                    if(newProduct.getCategory()!=null)
                        product.setCategory(newProduct.getCategory());
                    if(newProduct.getProductName()!=null)
                        product.setProductName(newProduct.getProductName());
                    if(newProduct.getPriceBrutto()!=null) {
                        product.setPriceNetto(newProduct.getPriceBrutto() / 1.23f);
                        product.setPriceBrutto(newProduct.getPriceBrutto());
                    }
                    return productRepository.save(product);
                }).orElseGet(() -> {
                    newProduct.setId(id);
                    return productRepository.save(newProduct);
                });
    }

    public void fellDb(){
        List<Product> products = new ArrayList<>();
        Product product = new Product(null, "ON", 4.40f, (float) Math.round(1.23f * 4.40f * 100) / 100, "Petrol", 2);
        addProduct(product);
        product = new Product(null, "95", 4.33f, (float) Math.round(1.23f * 4.33f * 100) / 100, "Petrol", 2);
        addProduct(product);
        product = new Product(null, "98", 4.50f, (float) Math.round(1.23f * 4.50f * 100) / 100, "Petrol",2);
        addProduct(product);
        product = new Product(null, "LPG", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "Petrol",1);
        addProduct(product);
        product = new Product(null, "Myjnia", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "car_wash",5);
        addProduct(product);
        product = new Product(null, "Myjnia z woskowaniem", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "car_wash",10);
        addProduct(product);
        product = new Product(null, "ON/Benzyna", 0f, 0f, "Loyalty_product",0);
        addProduct(product);

        saveProducts(products);
    }
}
