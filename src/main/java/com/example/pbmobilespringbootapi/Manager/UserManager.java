package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Admin;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.UserRepository;
import com.example.pbmobilespringbootapi.Enums.UserType;
import com.example.pbmobilespringbootapi.ErrorMessage;
import com.example.pbmobilespringbootapi.UserMini;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserManager {

    private UserRepository userRepository;
    private AddressManager addressManager;
    private CompanyManager companyManager;
    private AdminListManager adminListManager;

    @Autowired
    public UserManager(UserRepository userRepository, AddressManager addressManager, CompanyManager companyManager, AdminListManager adminListManager) {
        this.userRepository = userRepository;
        this.addressManager = addressManager;
        this.companyManager = companyManager;
        this.adminListManager = adminListManager;
    }

    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    public UserMini addUserReturnUserMini(User user) {
        User user1 = addUser(user);
        return new UserMini(user1.getId(), user1.getNickname(), user1.getPoints(), user1.getUserType());
    }

    public User addUser(User user) {
        User firstByUid = userRepository.findFirstByUid(user.getUid());
        User firstyByEmail = null;
        if (user.getEmail() != null && user.getEmail().length() > 0)
            userRepository.findFirstByEmail(user.getEmail());

        if (firstByUid != null)
            return firstByUid;
        else if (firstyByEmail != null)
            return firstyByEmail;

        Address address = user.getAddress();
        if (address != null) {
            Address address1 = addressManager.addAddress(address);
            user.setAddress(address1);
        } else {
            user.setAddress(null);
        }
        Company company = companyManager.verifyCompany(user.getCompany(), null);
        user.setCompany(company);
        if (user.getPoints() == null)
            user.setPoints(0);
        user.setUserType(checkTypeUser(user));
        return userRepository.save(user);
    }

    public Object loginUserMini(Long id) {
        Object loginObj = login(id);
        if (loginObj instanceof ErrorMessage)
            return loginObj;
        else {
            User user = (User) loginObj;
            return new UserMini(user.getId(), user.getNickname(), user.getPoints(), user.getUserType());
        }
    }

    public Object login(Long id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.LOGINFAILED.toString(), "Login failed!"));
            return errorMessage;
        } else
            return userOptional.get();
    }

    public User getUserById(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.orElse(null);
    }

    public Object updateUserData(User newUser, Long id) {
        Object checkUser = checkExist(id);
        if (checkUser instanceof ErrorMessage)
            return checkUser;

        return userRepository.findById(id)
                .map(user -> {
                    user.setNip(newUser.getNip());
                    user.setEmail(newUser.getEmail());
                    user.setNickname(newUser.getNickname());
                    user.setUid(newUser.getUid());
                    user.setAddress(addressManager.checkAddress(newUser.getAddress(), user.getAddress()));
                    user.setPoints(newUser.getPoints());
                    Company company = companyManager.verifyCompany(newUser.getCompany(), user.getCompany());
                    user.setCompany(company);
                    return userRepository.save(user);
                })
                .orElseGet(() -> {
                    newUser.setId(id);
                    return addUser(newUser);
                });
    }

    public Object deleteUser(Long id) {


        Object checkUser = checkExist(id);
        if (checkUser instanceof ErrorMessage)
            return checkUser;

        Optional<User> userOptional = userRepository.findById(id);
        User user = null;
        if (userOptional.isPresent())
            user = userOptional.get();
        else
            return false;

        Address address = user.getAddress();
        Company company = user.getCompany();
        userRepository.delete(user);
        if (address != null)
            addressManager.deleteAddress(address);
        if (company != null)
            companyManager.deleteCompany(company);

        return true;
    }

    public void addPoints(Long id, Integer points) {
        Object checkUser = checkExist(id);
        if (checkUser instanceof ErrorMessage)
            return;

        userRepository.findById(id)
                .map(user -> {
                    user.setPoints(user.getPoints() + points);
                    return userRepository.save(user);
                });
    }

    private Object checkExist(Long id) {
        Optional<User> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("User", id.toString());
        } else
            return true;
    }

    private String checkTypeUser(User user) {

        if (user.getEmail() == null || user.getEmail().length() == 0)
            return UserType.UNREGISTERED.toString();

        String userType = UserType.REGISTERED.toString();
        Iterable<Admin> admins = adminListManager.getAll();
        for (Admin admin : admins) {
            if (admin.getEmail().equals(user.getEmail())) {
                userType = admin.getUserType();
            }
        }
        return userType;
    }

    public Object getCompanyByUser(Long id) {
        User userById = getUserById(id);
        if (userById != null && userById.getCompany() != null)
            return userById.getCompany();
        else {
                return new Company();
        }
    }

    public void setCompanyToUser(Long idUser, Company newCompany){
        User user = getUserById(idUser);
        if(user!=null){
            user.setCompany(newCompany);
            updateUserData(user,idUser);
        }
    }

    public void fellDb() {
        User user = new User(null, "uid1", "Ziomus", "ziomus@gmail.com", "987654321", 0, UserType.OWNER.toString());
        Company company = new Company(null, "Firma1Name", "1234569879", "654654456", null);
        company.setAddress(new Address("Polska", "Małopolskie", "Kraków", "Nieznana", "13", "3", "32-354"));
        user.setCompany(company);
        user.setAddress(new Address("Polska", "Małopolska", "Kraków", "Pilotów", "12", "4", "32-333"));
        addUser(user);
    }

}
