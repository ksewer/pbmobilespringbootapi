package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.PositionOnReceipt;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.PositionOnReceiptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionOnReceiptManager {

    private PositionOnReceiptRepository positionOnReceiptRepository;
    private ProductManager productManager;

    @Autowired
    public PositionOnReceiptManager(PositionOnReceiptRepository positionOnReceiptRepository, ProductManager productManager) {
        this.positionOnReceiptRepository = positionOnReceiptRepository;
        this.productManager = productManager;
    }

    public PositionOnReceipt addPositionOnReceipt(PositionOnReceipt positionOnReceipt) {
        positionOnReceipt.setProduct(productManager.getProductById(positionOnReceipt.getProductId()));
        return positionOnReceiptRepository.save(positionOnReceipt);
    }

    public PositionOnReceipt savePositions(PositionOnReceipt positionOnReceipt) {
        positionOnReceipt.setProduct(productManager.getProductById(positionOnReceipt.getProductId()));
        return positionOnReceiptRepository.save(positionOnReceipt);
    }
}
