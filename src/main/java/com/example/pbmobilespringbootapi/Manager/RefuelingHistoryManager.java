package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.MagazinePosition;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Entity.RefuelingHistory;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.RefuelingHistoryRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
public class RefuelingHistoryManager {

    private RefuelingHistoryRepository refuelingHistoryRepository;
    private UserManager userManager;
    private MagazinePositionManager magazinePositionManager;
    private ProductManager productManager;

    @Autowired
    public RefuelingHistoryManager(RefuelingHistoryRepository refuelingHistoryRepository, UserManager userManager, MagazinePositionManager magazinePositionManager, ProductManager productManager) {
        this.refuelingHistoryRepository = refuelingHistoryRepository;
        this.userManager = userManager;
        this.magazinePositionManager = magazinePositionManager;
        this.productManager = productManager;
    }

    public Iterable<RefuelingHistory> getAllByUser(Long id) {
        User user = userManager.getUserById(id);
        if (user == null)
            return null;
        Iterable<RefuelingHistory> allByUser = refuelingHistoryRepository.findAllByUser(user);
        for (RefuelingHistory refuelingHistory : allByUser) {
            refuelingHistory.setDateRefueling(Date.from( refuelingHistory.getDate().atZone( ZoneId.systemDefault()).toInstant()));
        }
        return allByUser;
    }

    public Iterable<RefuelingHistory> getAll() {
        Iterable<RefuelingHistory> all = refuelingHistoryRepository.findAll();
        for (RefuelingHistory refuelingHistory : all) {
            refuelingHistory.setDateRefueling(Date.from( refuelingHistory.getDate().atZone( ZoneId.systemDefault()).toInstant()));
        }
        return all;
    }

    public Object add(RefuelingHistory refuelingHistory) {
        if (refuelingHistory.getUser() == null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "User is null!"));
            return errorMessage;
        }

        if (refuelingHistory.getProduct() == null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Product is null!"));
            return errorMessage;
        }

        User user = userManager.getUserById(refuelingHistory.getUser().getId());
        if (user == null)
            return ErrorMessage.generateNotFoundMessage("User", refuelingHistory.getUser().getId().toString());

        Product product = productManager.getProductById(refuelingHistory.getProduct().getId());
        if (product == null)
            return ErrorMessage.generateNotFoundMessage("Product", refuelingHistory.getUser().getId().toString());

        user.setPoints(user.getPoints() + refuelingHistory.getQuantity().intValue());
        userManager.updateUserData(user, user.getId());
        refuelingHistory.setUser(user);
        refuelingHistory.setProduct(product);
        if (refuelingHistory.getDate() == null) {
            refuelingHistory.setDate(LocalDateTime.now());
        }

//        int multiplierPoints = 0;
//        switch (Integer.parseInt(refuelingHistory.getProduct().getId().toString())) {
//            case 1:
//            case 2:
//            case 3:
//                multiplierPoints = 2;
//                break;
//            case 4:
//                multiplierPoints = 1;
//                break;
//        }
//        if (refuelingHistory.getPoints() == null) {
//            refuelingHistory.setPoints(refuelingHistory.getQuantity()*multiplierPoints);
//        }
        MagazinePosition magazinePosition = (MagazinePosition) magazinePositionManager.getQuantityByProductID(refuelingHistory.getProduct().getId());

        magazinePositionManager.updateQuantity(magazinePosition.getId(), refuelingHistory.getQuantity());
        return refuelingHistoryRepository.save(refuelingHistory);
    }

    public void fellDb() {
//        add(new RefuelingHistory(25f, LocalDateTime.now(), 35d, userManager.getUserById(1L), productManager.getProductById(1L)));
    }
}
