package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.PBLoyaltyProduct;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.PBLoyaltyProductRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PBLoyaltyProductManager {
    PBLoyaltyProductRepository pbLoyaltyProductRepository;
    ProductManager productManager;
    UserManager userManager;

    @Autowired
    public PBLoyaltyProductManager(PBLoyaltyProductRepository pbLoyaltyProductRepository, ProductManager productManager, UserManager userManager) {
        this.pbLoyaltyProductRepository = pbLoyaltyProductRepository;
        this.productManager = productManager;
        this.userManager = userManager;
    }

    public Iterable<PBLoyaltyProduct> getAllLoyaltyProduct() {
        return pbLoyaltyProductRepository.findAll();
    }

    public Object addLoyaltyProduct(Integer points, Long idProduct) {
        Product productFinded = productManager.getProductById(idProduct);
        PBLoyaltyProduct pbLoyaltyProduct = new PBLoyaltyProduct();
        if (productFinded != null) {
            pbLoyaltyProduct.setProduct(productFinded);
            pbLoyaltyProduct.setPoints(points);
            if (pbLoyaltyProductRepository.findFirstByProduct(productFinded) != null) {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.EXISTOTHER.toString(), "Another loylaty product is assigned to this product."));
                return errorMessage;
            } else {
                return pbLoyaltyProductRepository.save(pbLoyaltyProduct);
            }
        } else return null;
    }

    public void updateLoylatyProduct(Integer points, Long idProduct) {
        Product productById = productManager.getProductById(idProduct);
        if (productById == null)
            return;
        PBLoyaltyProduct pbLoyaltyProduct = pbLoyaltyProductRepository.findFirstByProduct(productById);
        if (pbLoyaltyProduct == null)
            return;

        pbLoyaltyProduct.setPoints(points);
        pbLoyaltyProduct.setProduct(productById);
        pbLoyaltyProductRepository.save(pbLoyaltyProduct);

    }

    public Object deleteLoyaltyProduct(Long id) {
        Optional<PBLoyaltyProduct> productOptional = pbLoyaltyProductRepository.findById(id);
        if (productOptional.isPresent()) {
            pbLoyaltyProductRepository.delete(productOptional.get());
            return true;
        } else {
            ErrorMessage errorMessage = ErrorMessage.generateNotFoundMessage("PBLoyaltyProduct", id.toString());
            return errorMessage;
        }
    }

    public PBLoyaltyProduct getLoylatyProductById(Long id) {
        Optional<PBLoyaltyProduct> productOptional = pbLoyaltyProductRepository.findById(id);
        return productOptional.orElse(null);
    }

    public Object changePointsToProduct(Long idUser, Long idLoyaltyProduct) {
        Optional<PBLoyaltyProduct> pbLoyaltyProductOptional = pbLoyaltyProductRepository.findById(idLoyaltyProduct);
        if (!pbLoyaltyProductOptional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("PBLoyaltyProduct", idLoyaltyProduct.toString());
        }
        Integer points = pbLoyaltyProductOptional.get().getPoints();
        User user = userManager.getUserById(idUser);
        if (user == null) {
            return ErrorMessage.generateNotFoundMessage("User", idLoyaltyProduct.toString());
        }
        if (user.getPoints() < points) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NOTENOUGHPOINTS.toString(), "Not enough points!"));
            return errorMessage;
        }

        user.setPoints(calculatePoints(user.getPoints(), points));
        userManager.updateUserData(user, idUser);
        return true;
    }

    public int calculatePoints(int userPoints, int pointsProduct) {
        if (userPoints >= pointsProduct)
            return userPoints - pointsProduct;
        else
            throw new IllegalArgumentException("Too low points to get this product");
    }

    public void fellDb() {
        addLoyaltyProduct(100, 7L);
        addLoyaltyProduct(50, 4L);
        addLoyaltyProduct(300, 5L);
        addLoyaltyProduct(400, 6L);
    }
}
