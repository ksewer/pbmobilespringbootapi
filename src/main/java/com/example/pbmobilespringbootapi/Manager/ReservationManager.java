package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Reservation;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.ReservationRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class ReservationManager {

    private ReservationRepository reservationRepository;
    private UserManager userManager;

    ObjectMapper mapper;

    @Autowired
    public ReservationManager(ReservationRepository reservationRepository, UserManager userManager, ObjectMapper mapper) {
        this.reservationRepository = reservationRepository;
        this.userManager = userManager;
        this.mapper = mapper;
    }

    public Iterable<Reservation> getAll(boolean isByUser, Long id) {
        checkActive();
        Iterable<Reservation> reservations = null;

        if (isByUser) {
            User user1 = userManager.getUserById(id);
            if (user1 != null) {
                reservations = reservationRepository.findAllByUser(user1);
            }
        } else
            reservations = reservationRepository.findAll();

        return reservations;
    }

    public Iterable<Reservation> getAllActive(boolean isByUser, Long id) {
        checkActive();
        Iterable<Reservation> reservations = null;

        if (isByUser) {
            User user1 = userManager.getUserById(id);
            if (user1 != null) {
                reservations = reservationRepository.findAllByUserAndActive(user1, true);
            }
        } else
            reservations = reservationRepository.findAllByActive(true);

        return reservations;
    }

    public Iterable<Reservation> getHistoryByUser(Long id) {
        checkActive();
        Iterable<Reservation> reservations = null;

        User user1 = userManager.getUserById(id);
        if (user1 != null) {
            reservations = reservationRepository.findAllByUserAndActive(user1, false);
        }

        return reservations;
    }

    public ObjectNode getImmediateAvailableTerm() {
        LocalDateTime localDateTime = LocalDateTime.now();

        List<String> availableTerm = getAvailableTerm(localDateTime.getDayOfMonth() + "." + localDateTime.getMonthValue() + "." + localDateTime.getYear());
        if (availableTerm != null && availableTerm.size() > 0) {
            return convertStringListToObjectNode(availableTerm.get(0));
        }
        return mapper.createObjectNode();
    }

    public List<ObjectNode> getAvailableTermObjectNode(String dateString) {
        return convertStringListToObjectNode(getAvailableTerm(dateString));
    }

    public List<String> getAvailableTerm(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat formatToSend = new SimpleDateFormat("HH:mm");
        boolean isToday = false;
        Date dateFromRequest;
        try {
            dateFromRequest = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFromRequest);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        dateFromRequest = calendar.getTime();
        Date dateNow = new Date();
        if (dateNow.getYear() == dateFromRequest.getYear()) {
            if (dateNow.getMonth() == dateFromRequest.getMonth()) {
                if (dateNow.getDay() == dateFromRequest.getDay()) {
                    isToday = true;
                }
            }
        }

        if (dateNow.compareTo(dateFromRequest) > 0) {
            return null;
        }

        int timeWashing = 29;
        Date dateStart = new Date();

        Iterable<Reservation> allByActive = reservationRepository.findAllByActive(true);
        Date dateSearch = new Date();
        dateSearch.setMinutes(30);
        if (!isToday) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(dateStart);
            calendar1.set(Calendar.HOUR_OF_DAY, 0);
            calendar1.set(Calendar.MINUTE, 30);
            dateStart = calendar1.getTime();
            dateSearch = calendar1.getTime();
        }
        if (dateStart.compareTo(dateSearch) > 0) {
            calendar.setTime(dateSearch);
            calendar.add(Calendar.MINUTE, 30);
            dateSearch = calendar.getTime();
        }
        List<String> availableTerms = new ArrayList<>();
        while (true) {
            if (dateSearch.getDay() != dateStart.getDay())
                break;
            boolean isExist = false;
            for (Reservation r : allByActive) {
                LocalDateTime dateOfReservation = convertDateToLocalDateTime(dateSearch);
                if ((dateOfReservation.compareTo(r.getDateOfReservation().minusMinutes(timeWashing)) > 0) && dateOfReservation.compareTo(r.getDateOfReservation().plusMinutes(timeWashing)) < 0) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                availableTerms.add(formatToSend.format(dateSearch));
            }

            calendar.setTime(dateSearch);
            calendar.add(Calendar.MINUTE, 30);
            dateSearch = calendar.getTime();
        }

        return availableTerms;
    }

    public void addReservation(Reservation newReservation, Long idUser) {
        if (newReservation == null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Reservation is null!"));
            return;// errorMessage;
        }
        User userById = userManager.getUserById(idUser);
        if (userById == null) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "User is null!"));
            return;// errorMessage;
        }
        if (newReservation.getDateOfReservation().compareTo(LocalDateTime.now().minusMinutes(15)) < 0) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.RESERVATIONINPAST.toString(), "You cannot reserve a car wash in the past!"));
            return;// errorMessage;
        }

        newReservation.setUser(userById);
        Iterable<Reservation> reservations = reservationRepository.findAll();
        LocalDateTime dateOfReservation = newReservation.getDateOfReservation();
        for (Reservation reservation : reservations) {
            int timeWashing = 20;
            if ((dateOfReservation.compareTo(reservation.getDateOfReservation().minusMinutes(timeWashing)) > 0) && dateOfReservation.compareTo(reservation.getDateOfReservation().plusMinutes(timeWashing)) < 0) {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.OTHERRESERVATIONTOTHISTERM.toString(), "Other reservation in this term!"));
                return;// errorMessage;
            }
        }

        User user = userManager.getUserById(newReservation.getUser().getId());
        if (user == null) {
            return;// ErrorMessage.generateNotFoundMessage("User", newReservation.getUser().getId().toString());
        }

        newReservation.setUser(user);
        newReservation.setDateofBooking(LocalDateTime.now());
        newReservation.setActive(true);
        reservationRepository.save(newReservation);

        return;
    }

    public Object deleteReservation(Long id) {
        Optional<Reservation> reservationFinded = reservationRepository.findById(id);
        if (reservationFinded.isPresent()) {
            reservationRepository.delete(reservationFinded.get());
            return true;
        } else {
            return ErrorMessage.generateNotFoundMessage("Reservation", id.toString());
        }
    }

    private void checkActive() {
        Iterable<Reservation> reservations = reservationRepository.findAll();
        for (Reservation reservation : reservations) {
            if (reservation.getDateOfReservation().compareTo(LocalDateTime.now()) < 0) {
                reservation.setActive(false);
                reservationRepository.save(reservation);
            }
        }
    }

    public void fellDb() {
//        addReservation(new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1), userManager.getUserById(1L)));
    }

    private Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private LocalDateTime convertDateToLocalDateTime(Date date) {
        LocalDateTime ldt = Instant.ofEpochMilli(date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        return ldt;
    }

    private List<ObjectNode> convertStringListToObjectNode(List<String> availableTerms) {
        List<ObjectNode> objectNodeList = new ArrayList<>();
        if(availableTerms == null)
            return null;
        for (String term : availableTerms) {
            ObjectNode objectNode = mapper.createObjectNode();
            objectNode.put("date", term);
            objectNodeList.add(objectNode);
        }
        return objectNodeList;
    }

    private ObjectNode convertStringListToObjectNode(String term) {
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("date", term);
        return objectNode;
    }
}
