package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.*;
import com.example.pbmobilespringbootapi.DAO.Repository.InvoiceRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class InvoiceManager {

    private InvoiceRepository invoiceRepository;
    private PositionOnReceiptManager positionOnReceiptManager;
    private UserManager userManager;
    private CompanyManager companyManager;
    private RefuelingHistoryManager refuelingHistoryManager;

    @Autowired
    public InvoiceManager(InvoiceRepository invoiceRepository, PositionOnReceiptManager positionOnReceiptManager, UserManager userManager, CompanyManager companyManager, RefuelingHistoryManager refuelingHistoryManager) {
        this.invoiceRepository = invoiceRepository;
        this.positionOnReceiptManager = positionOnReceiptManager;
        this.userManager = userManager;
        this.companyManager = companyManager;
        this.refuelingHistoryManager = refuelingHistoryManager;
    }

    public Invoice saveInvoice(Invoice invoice) {
        if (invoice.getPositionsOnReceipt() != null) {
            positionOnReceiptManager.savePositions(invoice.getPositionsOnReceipt());
        }
        if (invoice.getUserId() != null) {
            invoice.setUser(userManager.getUserById(invoice.getUserId()));
            invoice.setCompany(companyManager.verifyCompany(invoice.getUser().getCompany(),null));
        }
        if (invoice.getDate() == null) {
            invoice.setDate(LocalDateTime.now());
        }
        if (invoice.getCompany() != null) {
            invoice.setCompany(companyManager.verifyCompany(invoice.getCompany(), null));
        }
        Invoice invoiceSaved = invoiceRepository.save(invoice);
        addRefuelingHistory(invoiceSaved);
        return invoiceSaved;
    }

    public Iterable<Invoice> getAllInvoice() {
        return invoiceRepository.findAll();
    }

    public void fellDb() {
        Invoice invoice = new Invoice("przelew", null, companyManager.getCompanyById(1L), LocalDateTime.now(), 21, 1L);

        PositionOnReceipt position1 = new PositionOnReceipt(4L, 34.5f);
        invoice.setPositionOnReceipt(positionOnReceiptManager.addPositionOnReceipt(position1));


        saveInvoice(invoice);
    }

    public Object updatePaymentMethod(Long id, Invoice newInvoice) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return invoiceRepository.findById(id)
                .map(invoice -> {
                    if (newInvoice.getPaymentMethod() != null && newInvoice.getPaymentMethod().length() > 0)
                        invoice.setPaymentMethod(newInvoice.getPaymentMethod());
                    return invoiceRepository.save(invoice);
                });
    }

    public Object updateCompanyOnInvoice(Long id, Invoice newInvoice) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return invoiceRepository.findById(id)
                .map(invoice -> {
                    if (newInvoice.getCompany() != null)
                        invoice.setCompany(companyManager.verifyCompany(newInvoice.getCompany(), invoice.getCompany()));

                    return invoiceRepository.save(invoice);
                });
    }

    private Object checkExist(Long id) {
        Optional<Invoice> optional = invoiceRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("Invoice", id.toString());
        } else
            return true;
    }

    public void addRefuelingHistory(Invoice invoice) {
        String category = invoice.getPositionsOnReceipt().getProduct().getCategory();
        if (category.equals("Petrol")) {
            Integer points = invoice.getPositionsOnReceipt().getQuantity().intValue() * invoice.getPositionsOnReceipt().getProduct().getPoints();
            userManager.addPoints(invoice.getUser().getId(), points);
            User user = userManager.getUserById(invoice.getUser().getId());
            RefuelingHistory refuelingHistory = new RefuelingHistory(invoice.getPositionsOnReceipt().getQuantity(), LocalDateTime.now(), points, user, invoice.getPositionsOnReceipt().getProduct());
            refuelingHistoryManager.add(refuelingHistory);
        }
        if(category.equals("car_wash")){
            userManager.addPoints(invoice.getUser().getId(), invoice.getPositionsOnReceipt().getProduct().getPoints());
        }
    }
}
