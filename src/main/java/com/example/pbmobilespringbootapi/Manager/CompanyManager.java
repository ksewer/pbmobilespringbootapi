package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.CompanyRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyManager {

    private CompanyRepository companyRepository;
    private AddressManager addressManager;

    @Autowired
    public CompanyManager(CompanyRepository companyRepository, AddressManager addressManager) {
        this.companyRepository = companyRepository;
        this.addressManager = addressManager;
    }

    public Iterable<Company> getAllCompany() {
        return companyRepository.findAll();
    }

    public Company getCompanyByNip(String nip) {
        return companyRepository.findByNip(nip);
    }

    public Object getCompanyByRegon(String regon) {
        return companyRepository.findFirstByREGON(regon);
    }

    public Company getCompanyById(Long id) {
        Optional<Company> companyOptional = companyRepository.findById(id);
        return companyOptional.orElse(null);
    }

    public Object addCompany(Company company) {
        if (company != null && company.getNip() != null) {
            Company companyFinded = companyRepository.findByNip(company.getNip());
            if (companyFinded != null) {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.addMessage(new ErrorMessage.OneMessage("1001", "W bazie danych znajduje się firma z identycznym numerem NIP."));
                errorMessage.addMessage(new ErrorMessage.OneMessage("idCompany", companyFinded.getId().toString()));
                return errorMessage;
            } else {
                Address address = company.getAddress();
                if (address != null) {
                    Address address1 = addressManager.addAddress(address);
                    company.setAddress(address1);
                } else {
                    company.setAddress(null);
                }
                Company companySaved = companyRepository.save(company);
                return companySaved;
            }
        } else {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Company is null!"));
            return errorMessage;
        }
    }

    public Object updateCompanyData(Company newCompany, Long id) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return companyRepository.findById(id)
                .map(company -> {
                    company.setNip(newCompany.getNip());
                    company.setREGON(newCompany.getREGON());
                    company.setName(newCompany.getName());
                    company.setAddress(addressManager.checkAddress(newCompany.getAddress(), company.getAddress()));
                    return companyRepository.save(company);
                }).orElseGet(() -> {
                    newCompany.setId(id);
                    return companyRepository.save(newCompany);
                });
    }

    public Object deleteCompany(Long id) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        Optional<Company> companyOptional = companyRepository.findById(id);
        companyOptional.ifPresent(company -> {
            companyRepository.delete(company);
        });
        return true;
    }

    public void deleteCompany(Company company) {
        if (company != null) {
            companyRepository.delete(company);
            if (company.getAddress() != null) {
                addressManager.deleteAddress(company.getAddress());
            }
        }
    }

    private Company saveAddress(Company company) {
        Address address = company.getAddress();
        if (address != null) {
            Address address1 = addressManager.addAddress(address);
            company.setAddress(address1);
        } else {
            company.setAddress(null);
        }
        return company;
    }

    private Object checkExist(Long id) {
        Optional<Company> optional = companyRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("Company", id.toString());
        } else
            return true;
    }

    public Company verifyCompany(Company newCompany, Company oldCompany) {
        Object companyObject = null;

        if ((newCompany != null && newCompany.getId() != null)) {
            companyObject = checkExist(newCompany.getId());
            if (companyObject instanceof Company) {
                return (Company) companyObject;
            }
        }


        if (newCompany != null) {
            Company company1;
            if (oldCompany != null && oldCompany.getId() != null) {
                companyObject = updateCompanyData(newCompany, oldCompany.getId());
                if (companyObject instanceof Company)
                    company1 = (Company) companyObject;
                else
                    company1 = null;
            } else {
                companyObject = addCompany(newCompany);
                if (companyObject instanceof Company)
                    company1 = (Company) companyObject;
                else
                    company1 = null;
            }

            if (companyObject instanceof ErrorMessage) {
                String idString = ((ErrorMessage) companyObject).getMessageByNumber("idCompany");
                if (idString != null && idString.length() > 0) {
                    Company companyFinded = null;
                    try {
                        Long idComppany = Long.parseLong(idString);
                        companyFinded = getCompanyById(idComppany);
                    } catch (Exception ignored) {

                    }
                    if (companyFinded != null) {
                        company1 = companyFinded;
                    }
                }
            }
            return company1;
        } else {
            return null;
        }
    }
}
