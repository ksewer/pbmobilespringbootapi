package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.*;
import com.example.pbmobilespringbootapi.DAO.Repository.ReceiptRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@Service
public class ReceiptManager {

    private ReceiptRepository receiptRepository;
    private PositionOnReceiptManager positionOnReceiptManager;
    private UserManager userManager;
    private RefuelingHistoryManager refuelingHistoryManager;

    @Autowired
    public ReceiptManager(ReceiptRepository receiptRepository, PositionOnReceiptManager positionOnReceiptManager, UserManager userManager, RefuelingHistoryManager refuelingHistoryManager) {
        this.receiptRepository = receiptRepository;
        this.positionOnReceiptManager = positionOnReceiptManager;
        this.userManager = userManager;
        this.refuelingHistoryManager = refuelingHistoryManager;
    }

    public Receipt saveReceipt(Receipt receipt) {
        if (receipt.getPositionsOnReceipt() != null) {
            positionOnReceiptManager.savePositions(receipt.getPositionsOnReceipt());
        }
        if (receipt.getUserId() != null) {
            receipt.setUser(userManager.getUserById(receipt.getUserId()));
        }
        if (receipt.getDate() == null) {
            receipt.setDate(LocalDateTime.now());
        }
        Receipt receiptSaved = receiptRepository.save(receipt);
        addRefuelingHistory(receiptSaved);
        return receiptSaved;
    }

    public Iterable<Receipt> getAllReceipt() {
        return receiptRepository.findAll();
    }

    public void fellDb() {
        Receipt receipt = new Receipt("karta", "123456789", null, LocalDateTime.now(), 1L);

        PositionOnReceipt position1 = new PositionOnReceipt(1L, 20f);
        receipt.setPositionOnReceipt(positionOnReceiptManager.addPositionOnReceipt(position1));


        saveReceipt(receipt);
    }

    public Object updatePaymentMethod(Long id, Receipt newReceipt) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return receiptRepository.findById(id)
                .map(receipt -> {
                    if (newReceipt.getPaymentMethod() != null && newReceipt.getPaymentMethod().length() > 0)
                        receipt.setPaymentMethod(newReceipt.getPaymentMethod());
                    return receiptRepository.save(receipt);
                });
    }

    private Object checkExist(Long id) {
        Optional<Receipt> optional = receiptRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("Receipt", id.toString());
        } else
            return true;
    }

    public void addRefuelingHistory(Receipt receipt) {
        String category = receipt.getPositionsOnReceipt().getProduct().getCategory();
        if (category.equals("Petrol")) {
            Integer points = receipt.getPositionsOnReceipt().getQuantity().intValue() * receipt.getPositionsOnReceipt().getProduct().getPoints();
            userManager.addPoints(receipt.getUser().getId(), points);
            User user = userManager.getUserById(receipt.getUser().getId());
            RefuelingHistory refuelingHistory = new RefuelingHistory(receipt.getPositionsOnReceipt().getQuantity(), LocalDateTime.now(), points, user, receipt.getPositionsOnReceipt().getProduct());
            refuelingHistoryManager.add(refuelingHistory);
        }
        if(category.equals("car_wash")){
            userManager.addPoints(receipt.getUser().getId(), receipt.getPositionsOnReceipt().getProduct().getPoints());
        }
    }
}
