package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.AddressRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressManager {

    private AddressRepository addressRepository;

    @Autowired
    public AddressManager(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address addAddress(Address address) {
        Address addressCheck = addressRepository.findFirstByCountryAndCityAndStreetAndBuildingNumberAndApartmentNumberAndZipCode(address.getCountry(), address.getCity(), address.getStreet(), address.getBuildingNumber(), address.getApartmentNumber(), address.getZipCode());

        if(addressCheck != null)
            return addressCheck;

        return addressRepository.save(address);
    }

    public Iterable<Address> getAllAddress() {
        return addressRepository.findAll();
    }

    public Object updateAddress(Address newAddress, Long id) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        return addressRepository.findById(id)
                .map(address -> {
                    address.setApartmentNumber(newAddress.getApartmentNumber());
                    address.setBuildingNumber(newAddress.getBuildingNumber());
                    address.setCity(newAddress.getCity());
                    address.setCountry(newAddress.getCountry());
                    address.setStreet(newAddress.getStreet());
                    address.setVoivodeship(newAddress.getVoivodeship());
                    address.setZipCode(newAddress.getZipCode());
                    return addressRepository.save(address);
                }).orElseGet(() -> {
                    newAddress.setId(id);
                    return addressRepository.save(newAddress);
                });
    }

    public void deleteAddress(Address address) {
        addressRepository.delete(address);
    }

    public Object deleteAddress(Long id) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        Optional<Address> addressOptional = addressRepository.findById(id);
        addressOptional.ifPresent(address -> addressRepository.delete(address));
        return true;
    }

    public Object getAddressById(Long id) {
        Object check = checkExist(id);
        if (check instanceof ErrorMessage)
            return check;
        Optional<Address> addressOptional = addressRepository.findById(id);
        return addressOptional.orElse(null);
    }

    private Object checkExist(Long id) {
        Optional<Address> optional = addressRepository.findById(id);
        if (!optional.isPresent()) {
            return ErrorMessage.generateNotFoundMessage("Address", id.toString());
        } else
            return true;
    }

    public Address checkAddress(Address addressNew, Address addressOld) {
        if (addressNew != null) {
            Address address1;
            if (addressOld != null) {
                Object addressObj = updateAddress(addressNew, addressOld.getId());
                if (addressObj instanceof Address)
                    address1 = (Address) addressObj;
                else
                    address1 = null;
            } else {
                address1 = addAddress(addressNew);
            }
            return address1;
        } else {
            return null;
        }
    }

}
