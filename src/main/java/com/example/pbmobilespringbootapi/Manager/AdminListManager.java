package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Admin;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.AdminListRepository;
import com.example.pbmobilespringbootapi.Enums.UserType;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminListManager {
    private AdminListRepository adminListRepository;
    private ProductManager productManager;

    @Autowired
    public AdminListManager(AdminListRepository adminListRepository, ProductManager productManager) {
        this.adminListRepository = adminListRepository;
        this.productManager = productManager;
    }

    public Object addAdmin(Admin admin) {
        if (admin.getUserType() == null && admin.getUserType().length() == 0) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "UserType is null!"));
            return errorMessage;
        }

        Admin adminFinded = null;
        if (admin != null)
            adminFinded = adminListRepository.findFirstByEmail(admin.getEmail());
        else {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Admin is null!"));
            return errorMessage;
        }
        if (adminFinded != null)
            return adminFinded;
        else return adminListRepository.save(admin);
    }

    public Object checkEmail(Admin admin) {
        Admin adminFinded = null;
        if (admin != null)
            adminFinded = adminListRepository.findFirstByEmail(admin.getEmail());
        else {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString(), "Admin is null!"));
            return errorMessage;
        }
        if (adminFinded != null)
            return adminFinded;
        else {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage.addMessage(new ErrorMessage.OneMessage(ErrorMessage.ErrorNumberConst.NOTFOUNDED.toString(), "Admin with email " + admin.getEmail() + " not exist!"));
            return errorMessage;
        }
    }

    public Object deleteAdmin(Long id) {
        Optional<Admin> adminFinded = adminListRepository.findById(id);
        if (adminFinded.isPresent()) {
            adminListRepository.delete(adminFinded.get());
            return true;
        } else {
            return ErrorMessage.generateNotFoundMessage("Admin", id.toString());
        }
    }

    public void updateProductPrice(Long idProduct, Float price) {
        Product productById = productManager.getProductById(idProduct);
        if(productById!=null){
            productById.setPriceBrutto(price);
            productById.setPriceNetto(price/1.23f);
            productManager.saveProduct(productById);
        }
    }

    public Iterable<Admin> getAll() {
        return adminListRepository.findAll();
    }

    public void fellDb() {
        addAdmin(new Admin("romek12ziomek@gmail.com", UserType.OWNER.toString()));
        addAdmin(new Admin("szajna.marcin12@gmail.com", UserType.WORKER.toString()));
    }
}
