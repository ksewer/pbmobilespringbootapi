package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Entity.InfoApp;
import com.example.pbmobilespringbootapi.DAO.Repository.InfoAppRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import com.example.pbmobilespringbootapi.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InfoAppManager {

    private InfoAppRepository infoAppRepository;
    private AddressManager addressManager;
    private CompanyManager companyManager;

    @Autowired
    public InfoAppManager(InfoAppRepository infoAppRepository, AddressManager addressManager, CompanyManager companyManager) {
        this.infoAppRepository = infoAppRepository;
        this.addressManager = addressManager;
        this.companyManager = companyManager;
    }

    private InfoApp add(InfoApp infoApp) {
        //company
            if (infoApp.getCompany() != null) {
                Object companyObj = companyManager.addCompany(infoApp.getCompany());
                if (companyObj instanceof Company)
                    infoApp.setCompany((Company) companyObj);
            }

            //address
                if (infoApp.getAddress() != null) {
                    infoApp.setAddress(addressManager.addAddress(infoApp.getAddress()));
                } else {
                    Address addressFromNewInfoApp = infoApp.getAddress();
                    infoApp.getCompany().setAddress(addressManager.addAddress(addressFromNewInfoApp));
                }

        return infoAppRepository.save(infoApp);
    }

    public Object updateInfo(InfoApp newInfoApp) {
        return infoAppRepository.findById(1L)
                .map(infoApp -> {
                    infoApp.setOwnerName(newInfoApp.getOwnerName());
                    infoApp.setTelephoneNumber(newInfoApp.getTelephoneNumber());

                    //company
                    if (newInfoApp.getCompany() != null) {
                        if (infoApp.getCompany() != null) {
                            Object companyObj = companyManager.updateCompanyData(newInfoApp.getCompany(), infoApp.getCompany().getId());
                            if (companyObj instanceof Company)
                                infoApp.setCompany((Company) companyObj);
                        }

                        //address
                        if (newInfoApp.getAddress() != null) {
                            if (infoApp.getAddress() != null) {
                                addressManager.updateAddress(newInfoApp.getAddress(), infoApp.getAddress().getId());
                            } else {
                                Address addressFromNewInfoApp = newInfoApp.getAddress();
                                infoApp.getCompany().setAddress(addressManager.addAddress(addressFromNewInfoApp));
                            }
                        }
                    }
                    return infoAppRepository.save(infoApp);
                })
                .orElseGet(() -> {
                    newInfoApp.setId(1L);
                    return add(newInfoApp);
                });
    }

    public Object getInfo() {
        Optional<InfoApp> infoAppOptional = infoAppRepository.findById(1L);
        if (infoAppOptional.isPresent()) {
            InfoApp infoApp = infoAppOptional.get();
            infoApp.setWorkers();
            return infoApp;
        } else {
            return ErrorMessage.generateNotFoundMessage("InfoApp", "1");
        }
    }

    public void fellDb(){
        Address address = new Address();
        Company company = new Company();
        address.setCountry("Polska");
        address.setVoivodeship("Małopolska");
        address.setCity("Kraków");
        address.setZipCode("31-864");
        address.setStreet("Jana Pawła II");
        address.setBuildingNumber("37");

        company.setAddress(new Address());
        company.getAddress().setCountry("Polska");
        company.getAddress().setVoivodeship("Małopolska");
        company.getAddress().setCity("Kraków");
        company.getAddress().setZipCode("31-864");
        company.getAddress().setStreet("Jana Pawła II");
        company.getAddress().setBuildingNumber("37");
        company.setNip("9856473812");
        company.setREGON("75698775");
        company.setName("PB");
        InfoApp infoApp = new InfoApp(address, company, "(070) 012-34-56", "Jan Kowalski");
        updateInfo(infoApp);
    }
}
