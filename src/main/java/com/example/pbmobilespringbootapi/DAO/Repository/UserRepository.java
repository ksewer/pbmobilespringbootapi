package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findFirstByUid(String uid);
    User findFirstByEmail(String email);
}
