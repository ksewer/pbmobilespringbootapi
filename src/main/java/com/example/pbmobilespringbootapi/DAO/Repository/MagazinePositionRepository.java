package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.MagazinePosition;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface MagazinePositionRepository extends CrudRepository<MagazinePosition, Long> {
    MagazinePosition findFirstByProduct(Product product);
}
