package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.Admin;
import org.springframework.data.repository.CrudRepository;

public interface AdminListRepository extends CrudRepository<Admin, Long> {
    Admin findFirstByEmail(String email);
}
