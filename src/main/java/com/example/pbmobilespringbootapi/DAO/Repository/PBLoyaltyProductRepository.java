package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.PBLoyaltyProduct;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface PBLoyaltyProductRepository extends CrudRepository<PBLoyaltyProduct, Long> {
    PBLoyaltyProduct findFirstByProduct(Product product);
}
