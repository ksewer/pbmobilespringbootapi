package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.PositionOnReceipt;
import org.springframework.data.repository.CrudRepository;

public interface PositionOnReceiptRepository extends CrudRepository<PositionOnReceipt, Long> {
}
