package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.Receipt;
import org.springframework.data.repository.CrudRepository;

public interface ReceiptRepository extends CrudRepository<Receipt, Long> {
}
