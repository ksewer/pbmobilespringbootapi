package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.RefuelingHistory;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import org.springframework.data.repository.CrudRepository;

public interface RefuelingHistoryRepository extends CrudRepository<RefuelingHistory, Long> {
    Iterable<RefuelingHistory> findAllByUser(User user);
}
