package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.InfoApp;
import org.springframework.data.repository.CrudRepository;

public interface InfoAppRepository extends CrudRepository<InfoApp, Long> {
}
