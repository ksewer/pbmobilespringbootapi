package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Long> {
    List<Address> findAllByStreet(String street);

    Address findFirstByCountryAndCityAndStreetAndBuildingNumberAndApartmentNumberAndZipCode(String country, String city, String street, String buildingNumber, String apartmentNumber, String zipCode);
}
