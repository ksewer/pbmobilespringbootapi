package com.example.pbmobilespringbootapi.DAO.Repository;

import com.example.pbmobilespringbootapi.DAO.Entity.Reservation;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    Iterable<Reservation> findAllByUser(User user);

    Iterable<Reservation> findAllByUserAndActive(User user, boolean active);

    Iterable<Reservation> findAllByActive(boolean active);
}
