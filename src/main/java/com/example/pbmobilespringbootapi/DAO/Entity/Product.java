package com.example.pbmobilespringbootapi.DAO.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String productName;
    private Float priceNetto;
    private Float priceBrutto;
    private String category;
    @JsonIgnore
    private Integer points;

    public Product(Long id, String productName, Float priceNetto, Float priceBrutto, String category, Integer points) {
        this.id = id;
        this.productName = productName;
        this.priceNetto = priceNetto;
        this.priceBrutto = priceBrutto;
        this.category = category;
        this.points = points;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getPriceNetto() {
        return priceNetto;
    }

    public void setPriceNetto(Float priceNetto) {
        this.priceNetto = priceNetto;
    }

    public Float getPriceBrutto() {
        return priceBrutto;
    }

    public void setPriceBrutto(Float priceBrutto) {
        this.priceBrutto = priceBrutto;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", priceNetto=" + priceNetto +
                ", priceBrutto=" + priceBrutto +
                ", category='" + category + '\'' +
                '}';
    }
}
