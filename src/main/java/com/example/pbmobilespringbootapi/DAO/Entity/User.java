package com.example.pbmobilespringbootapi.DAO.Entity;

import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uid;
    private String nickname;
    private String email;
    private String nip;
    private Integer points;
    private String userType;
    @ManyToOne
    private Address address;
    @ManyToOne
    private Company company;

    public User() {
    }

    public User(Long id, String uid, String nickname, String email, String nip, Integer points, String userType) {
        this.id = id;
        this.uid = uid;
        this.nickname = nickname;
        this.email = email;
        this.nip = nip;
        this.points = points;
        this.userType = userType;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String token) {
        this.uid = token;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", nip='" + nip + '\'' +
                ", points=" + points +
                ", userType='" + userType + '\'' +
                ", address=" + address +
                ", company=" + company +
                '}';
    }
}
