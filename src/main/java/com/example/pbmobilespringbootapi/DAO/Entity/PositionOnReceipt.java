package com.example.pbmobilespringbootapi.DAO.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class PositionOnReceipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Float quantity;

    @ManyToOne
    private Product product;


    @JsonIgnore
    private Long productIdFromApp;

    public PositionOnReceipt(Long productId, Float quantity) {
        this.productIdFromApp = productId;
        this.quantity = quantity;
    }

    public PositionOnReceipt() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getProductId() {
        return productIdFromApp;
    }

    public void setProductId(Long productId) {
        this.productIdFromApp = productId;
    }
}
