package com.example.pbmobilespringbootapi.DAO.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String paymentMethod;
    private LocalDateTime date;
    private int paymentTermDay;

    @JsonIgnore
    private Long userIdFromApp;

    @ManyToOne
    private PositionOnReceipt positionsOnReceipt;

    @JsonIgnore
    @ManyToOne
    private User user;

    @ManyToOne
    private Company company;

    public Invoice(String paymentMethod, User user, Company company, LocalDateTime date, int paymentTermDay, Long userId) {
        this.paymentMethod = paymentMethod;
        this.user = user;
        this.date = date;
        this.paymentTermDay = paymentTermDay;
        this.company = company;
        this.userIdFromApp = userId;
    }

    public Invoice(String paymentMethod, Company company, LocalDateTime date, int paymentTermDay) {
        this.paymentMethod = paymentMethod;
        this.date = date;
    }

    public Invoice() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PositionOnReceipt getPositionsOnReceipt() {
        return positionsOnReceipt;
    }

    public void setPositionOnReceipt(PositionOnReceipt positionsOnReceipt) {
        this.positionsOnReceipt = positionsOnReceipt;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getUserId() {
        return userIdFromApp;
    }

    public void setUserId(Long userId) {
        this.userIdFromApp = userId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public int getPaymentTermDay() {
        return paymentTermDay;
    }

    public void setPaymentTermDay(int paymentTermDay) {
        this.paymentTermDay = paymentTermDay;
    }
}
