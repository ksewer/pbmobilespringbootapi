package com.example.pbmobilespringbootapi.DAO.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class RefuelingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Float quantity;
    @JsonIgnore
    LocalDateTime date;
    Integer points;
    @Transient
    Date dateRefueling;

    @JsonIgnore
    @ManyToOne
    User user;

    @ManyToOne
    Product product;

    public RefuelingHistory(Float quantity, LocalDateTime date, Integer points, User user, Product product) {
        this.quantity = quantity;
        this.date = date;
        this.points = points;
        this.user = user;
        this.product = product;
    }

    public RefuelingHistory() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Date getDateRefueling() {
        return dateRefueling;
    }

    public void setDateRefueling(Date dateRefueling) {
        this.dateRefueling = dateRefueling;
    }
}
