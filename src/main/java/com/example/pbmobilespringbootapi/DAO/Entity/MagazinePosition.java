package com.example.pbmobilespringbootapi.DAO.Entity;

import javax.persistence.*;

@Entity
public class MagazinePosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Float quantity;

    @ManyToOne
    private Product product;

    public MagazinePosition(Product product, Float quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public MagazinePosition() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
