package com.example.pbmobilespringbootapi.DAO.Entity;

import com.example.pbmobilespringbootapi.Worker;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class InfoApp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @OneToOne
    private Address address;

    @OneToOne
    private Company company;

    String telephoneNumber;
    String ownerName;
    @Transient
    private List<Worker> workers;

    public InfoApp(Address address, Company company, String telephoneNumber, String ownerName) {
        this.address = address;
        this.company = company;
        this.telephoneNumber = telephoneNumber;
        this.ownerName = ownerName;
    }

    public InfoApp() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers() {
        workers = new ArrayList<>();
        workers.add(new Worker("Tomasz Nowak", "Kasjer"));
        workers.add(new Worker("Karol Kowalczyk", "Kasjer"));
        workers.add(new Worker("Wincenty Raczek", "Monitoring"));
        workers.add(new Worker("Eugeniusz Żuczek", "Monitoring"));
        workers.add(new Worker("Alojzy Mydlak", "Myjnia"));
        workers.add(new Worker("Baltazar Gąbka", "Myjnia"));
        workers.add(new Worker("Czesław Ratajczak", "Obsługa dystrybutora LPG"));
        workers.add(new Worker("Radosław Rataj", "Obsługa dystrybutora LPG"));
    }
}
