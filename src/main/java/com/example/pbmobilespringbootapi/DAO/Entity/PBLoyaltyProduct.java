package com.example.pbmobilespringbootapi.DAO.Entity;

import javax.persistence.*;

@Entity
public class PBLoyaltyProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Integer points;

    @ManyToOne
    Product product;

    public PBLoyaltyProduct(Long id, Integer points, Product product) {
        this.points = points;
        this.product = product;
        this.id = id;
    }

    public PBLoyaltyProduct() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
