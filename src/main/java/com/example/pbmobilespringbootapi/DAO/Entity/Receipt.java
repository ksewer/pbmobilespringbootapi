package com.example.pbmobilespringbootapi.DAO.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String paymentMethod;
    @JsonIgnore
    private String nip;
    private LocalDateTime date;

    @JsonIgnore
    private Long userIdFromApp;

    @ManyToOne
    private PositionOnReceipt positionsOnReceipt;

    @JsonIgnore
    @ManyToOne
    private User user;

    public Receipt(String paymentMethod, String nip, User user, LocalDateTime date,Long userId) {
        this.paymentMethod = paymentMethod;
        this.nip = nip;
        this.user = user;
        this.date = date;
        this.userIdFromApp = userId;
    }

    public Receipt() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userIdFromApp;
    }

    public void setUserId(Long userId) {
        this.userIdFromApp = userId;
    }

    public PositionOnReceipt getPositionsOnReceipt() {
        return positionsOnReceipt;
    }

    public void setPositionOnReceipt(PositionOnReceipt positionsOnReceipt) {
        this.positionsOnReceipt = positionsOnReceipt;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setPositionsOnReceipt(PositionOnReceipt positionsOnReceipt) {
        this.positionsOnReceipt = positionsOnReceipt;
    }
}
