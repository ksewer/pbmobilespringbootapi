package com.example.pbmobilespringbootapi.DAO.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    LocalDateTime dateofBooking;
    LocalDateTime dateOfReservation;
    String type;

    @JsonIgnore
    @ManyToOne
    User user;

    @JsonIgnore
    boolean active;

    public Reservation(LocalDateTime dateofBooking, LocalDateTime dateOfReservation, Long idUser, String type) {
        this.dateofBooking = dateofBooking;
        this.dateOfReservation = dateOfReservation;
        this.type = type;
        active = true;
        this.user = new User();
        this.user.setId(idUser);
    }

    public Reservation(Long id, LocalDateTime dateofBooking, LocalDateTime dateOfReservation, Long idUser, String type) {
        this.dateofBooking = dateofBooking;
        this.dateOfReservation = dateOfReservation;
        this.type = type;
        active = true;
        this.user = new User();
        this.user.setId(idUser);
        this.id = id;
    }

    public Reservation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateofBooking() {
        return dateofBooking;
    }

    public void setDateofBooking(LocalDateTime dateofBooking) {
        this.dateofBooking = dateofBooking;
    }

    public LocalDateTime getDateOfReservation() {
        return dateOfReservation;
    }

    public void setDateOfReservation(LocalDateTime dateOfReservation) {
        this.dateOfReservation = dateOfReservation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
