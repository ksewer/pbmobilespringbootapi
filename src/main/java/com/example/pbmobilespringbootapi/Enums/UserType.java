package com.example.pbmobilespringbootapi.Enums;

public enum UserType {
    REGISTERED("REGISTERED"),
    UNREGISTERED("UNREGISTERED"),
    OWNER("OWNER"),
    WORKER("WORKER");

    private String number;

    UserType(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return number;
    }
}
