package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.PBLoyaltyProduct;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.PBLoyaltyProductRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PBLoyaltyProductManagerTest {

    @Mock
    PBLoyaltyProductRepository pbLoyaltyProductRepository;

    @Mock
    UserManager userManager;

    @InjectMocks
    PBLoyaltyProductManager pbLoyaltyProductManager;

    @Before
    public void init(){
        given(pbLoyaltyProductRepository.findAll()).willReturn(prepareMockData());
        given(pbLoyaltyProductRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(prepareMockData().get(0)));

        User user = new User();
        user.setPoints(1000);
        user.setId(1L);
        given(userManager.getUserById(1L)).willReturn(user);
    }

    @Test
    public void getAllLoyaltyProduct() {
        List<PBLoyaltyProduct> products = (List) pbLoyaltyProductManager.getAllLoyaltyProduct();
        Assert.assertThat(products, Matchers.hasSize(4));
    }

    @Test
    public void getLoylatyProductById() {
        PBLoyaltyProduct product = pbLoyaltyProductManager.getLoylatyProductById(1L);
        Assert.assertNotNull(product);
        Assert.assertEquals(product.getId().intValue(),1L);
    }

    @Test
    public void changePointsToProduct(){
        Object o = pbLoyaltyProductManager.changePointsToProduct(1L, 1L);
        boolean isOk = false;
        if(!(o instanceof ErrorMessage))
            isOk = true;
        Assert.assertTrue(isOk);
    }

    @Test
    public void calculatePoints(){
        int points = pbLoyaltyProductManager.calculatePoints(1000, 100);
        Assert.assertEquals(points,900);
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            pbLoyaltyProductManager.calculatePoints(90, 100);
        });
        assertTrue(exception.getMessage().contains("Too low points to get this product"));
    }

    public List<PBLoyaltyProduct> prepareMockData(){
        List<PBLoyaltyProduct> products = new ArrayList<>();
        PBLoyaltyProduct product = new PBLoyaltyProduct(1L,100, ProductManagerTest.prepareMockData().get(0));
        products.add(product);
        product = new PBLoyaltyProduct(2L,50, ProductManagerTest.prepareMockData().get(3));
        products.add(product);
        product = new PBLoyaltyProduct(3L,300, ProductManagerTest.prepareMockData().get(4));
        products.add(product);
        product = new PBLoyaltyProduct(4L,400, ProductManagerTest.prepareMockData().get(5));
        products.add(product);
        return products;
    }
}