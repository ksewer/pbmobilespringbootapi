package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Reservation;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.ReservationRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ReservationManagerTest {

    @Mock
    public ReservationRepository reservationRepository;
    @Spy
    public ObjectMapper mapper;
    @Mock
    public UserManager userManager;

    @InjectMocks
    public ReservationManager reservationManager;

    public Long userId = 1L;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        User user = new User();
        user.setId(userId);

        given(reservationRepository.findAll()).willReturn(prepareMockData());
        given(reservationRepository.findAllByUser(user)).willReturn(reservationsByUser(userId));
        given(userManager.getUserById(userId)).willReturn(user);
        given(reservationRepository.findAllByUserAndActive(user, true)).willReturn(findAllByUserAndActive(user, true));
        given(reservationRepository.findAllByActive(true)).willReturn(findAllByActive(true));
//        given(mapper.createObjectNode()).willReturn(mapper.createObjectNode());
    }

    @Test
    public void getAll() {
        List<Reservation> all = (List<Reservation>) reservationManager.getAll(false, userId);
        Assert.assertThat(all, Matchers.hasSize(5));
        all = (List<Reservation>) reservationManager.getAll(true, userId);
        Assert.assertThat(all, Matchers.hasSize(3));
    }

    @Test
    public void getAllActive() {
        List<Reservation> all = (List<Reservation>) reservationManager.getAllActive(false, userId);
        Assert.assertThat(all, Matchers.hasSize(4));
        all = (List<Reservation>) reservationManager.getAllActive(true, userId);
        Assert.assertThat(all, Matchers.hasSize(2));
    }

    @Test
    public void getImmediateAvailableTerm() {
        JsonNode jsonNode = reservationManager.getImmediateAvailableTerm();
        Assert.assertNotNull(jsonNode);
    }

    @Test
    public void addReservation() {
        Long idReservation = 6L;
        Reservation reservation = new Reservation(idReservation, LocalDateTime.now(), LocalDateTime.now().plusDays(1), 1L, "Myjnia z woskiem");
        reservationManager.addReservation(reservation, userId);
    }

    @Test
    public void deleteReservation() {
        Long idReservation = 1L;
        reservationManager.deleteReservation(idReservation);
    }

    public List<Reservation> prepareMockData() {
        List<Reservation> reservations = new ArrayList<>();
        reservations.add(new Reservation(1L, LocalDateTime.now(), LocalDateTime.now().plusDays(1), 1L, "Myjnia z woskiem"));
        reservations.add(new Reservation(2L, LocalDateTime.now(), LocalDateTime.now().plusDays(2), 2L, "Myjnia z woskiem"));
        reservations.add(new Reservation(3L, LocalDateTime.now(), LocalDateTime.now().plusDays(3), 1L, "Myjnia"));
        reservations.add(new Reservation(4L, LocalDateTime.now(), LocalDateTime.now().plusDays(4), 2L, "Myjnia"));
        Reservation reservation = new Reservation(5L, LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, "Myjnia z woskiem");
        reservation.setActive(false);
        reservations.add(reservation);
        return reservations;
    }

    public List<Reservation> reservationsByUser(Long id) {
        List<Reservation> reservations = new ArrayList<>();
        for (Reservation reservation : prepareMockData()) {
            if (reservation.getUser().getId() == id)
                reservations.add(reservation);
        }
        return reservations;
    }

    public List<Reservation> findAllByUserAndActive(User user, boolean isActive) {
        List<Reservation> reservations = new ArrayList<>();
        for (Reservation reservation : prepareMockData()) {
            if (reservation.getUser().getId() == user.getId() && reservation.isActive() == isActive)
                reservations.add(reservation);
        }
        return reservations;
    }

    public List<Reservation> findAllByActive(boolean isActive) {
        List<Reservation> reservations = new ArrayList<>();
        for (Reservation reservation : prepareMockData()) {
            if (reservation.isActive() == isActive)
                reservations.add(reservation);
        }
        return reservations;
    }
}