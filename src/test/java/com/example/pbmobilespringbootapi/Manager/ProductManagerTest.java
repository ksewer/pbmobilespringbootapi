package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.ProductRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ProductManagerTest {

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductManager productManager;

    @Before
    public void init() {
        given(productRepository.findAll()).willReturn(prepareMockData());
        given(productRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(prepareMockData().get(0)));
        given(productRepository.findAllByCategory("Petrol")).willReturn(findAllByCategory("Petrol"));
    }

    @Test
    public void getAllProducts() {
        List<Product> products = (List)productManager.getAllProducts();
        Assert.assertThat(products, Matchers.hasSize(7));
    }

    @Test
    public void getProductById() {
        Product product = productManager.getProductById(1L);
        Assert.assertNotNull(product);
        Long id = product.getId();
        Assert.assertEquals(id.intValue(), 1);
    }

    @Test
    public void getProductsByCategory() {
        List<Product> products = productManager.getProductsByCategory("Petrol");
        Assert.assertThat(products,Matchers.hasSize(4));
    }

    public static List<Product> prepareMockData() {
        List<Product> products = new ArrayList<>();
        Product product = new Product(1L, "ON", 4.40f, (float) Math.round(1.23f * 4.40f * 100) / 100, "Petrol", 2);
        products.add(product);
        product = new Product(2L, "95", 4.33f, (float) Math.round(1.23f * 4.33f * 100) / 100, "Petrol", 2);
        products.add(product);
        product = new Product(3L, "98", 4.50f, (float) Math.round(1.23f * 4.50f * 100) / 100, "Petrol", 2);
        products.add(product);
        product = new Product(4L, "LPG", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "Petrol", 1);
        products.add(product);
        product = new Product(5L, "Myjnia", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "car_wash", 5);
        products.add(product);
        product = new Product(6L, "Myjnia z woskowaniem", 1.83f, (float) Math.round(1.23f * 1.83f * 100) / 100, "car_wash", 10);
        products.add(product);
        product = new Product(7L, "ON/Benzyna", 0f, 0f, "Loyalty_product", 0);
        products.add(product);

        return products;
    }

    public List<Product> findAllByCategory(String category) {
        List<Product> products = new ArrayList<>();
        for (Product product : prepareMockData()) {
            if (product.getCategory().equals(category))
                products.add(product);
        }

        return products;
    }

}