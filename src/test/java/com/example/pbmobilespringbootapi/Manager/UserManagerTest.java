package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.UserRepository;
import com.example.pbmobilespringbootapi.Enums.UserType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class UserManagerTest {

    @Mock
    public UserRepository userRepository;

    @InjectMocks
    public AddressManager addressManager;

    @InjectMocks
    public CompanyManager companyManager;

    @InjectMocks
    public AdminListManager adminListManager;

    @InjectMocks
    public UserManager userManager;


    Long userId = 1L;
    String email = "ziomus3@gmail.com";
    String uid = "uid2";
    List<User> users = null;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        given(userRepository.findById(userId)).willReturn(findById(userId));
        given(userRepository.findAll()).willReturn(prepareMockDataUserManager());
    }

    @Test
    public void getAllUsers() {
        userManager.getAllUsers();
    }

    @Test
    public void login() {
        User user = prepareMockDataUserManager().get(0);
        Assert.assertEquals(user,userManager.login(userId));
        Assert.assertNotEquals(user,userManager.login(2L));
    }

    @Test
    public void getUserById() {
        Long idFindedUser = 1L;
        User userActual = prepareMockDataUserManager().get(idFindedUser.intValue()-1);
        User userById = userManager.getUserById(idFindedUser);
        Assert.assertEquals(userActual, userById);

        idFindedUser = 2L;
        userActual = prepareMockDataUserManager().get(1);
        userById = userManager.getUserById(idFindedUser);
        Assert.assertNotEquals(userActual, userById);
    }

    @Test
    public void deleteUser() {
        userManager.deleteUser(2L);
    }

    @Test
    public void addPoints() {
        userManager.addPoints(1L,100);

    }

    @Test
    public void getCompanyByUser() {
        Company companyActual = prepareMockDataUserManager().get(0).getCompany();
        Company company = null;
        Object obj = userManager.getCompanyByUser(userId);

        if(obj instanceof Company)
            company = (Company) obj;

        Assert.assertEquals(company,companyActual);
    }

    public List<User> prepareMockDataUserManager(){
        if(users == null) {
            users = new ArrayList<>();
            User user = new User(1L, "uid1", "Ziomus1", "ziomus1@gmail.com", "987654321", 0, UserType.OWNER.toString());
            Company company = new Company(1L, "Firma1Name", "1234569879", "654654456", null);
            user.setCompany(company);
            users.add(user);
            users.add(new User(2L, "uid2", "Ziomus2", "ziomus2@gmail.com", "987654322", 0, UserType.WORKER.toString()));
            users.add(new User(3L, "uid3", "Ziomus3", "ziomus3@gmail.com", "987654323", 0, UserType.OWNER.toString()));
            users.add(new User(4L, "uid4", "Ziomus4", "ziomus4@gmail.com", "987654324", 0, UserType.WORKER.toString()));
            users.add(new User(5L, "uid5", "Ziomus5", "ziomus5@gmail.com", "987654325", 0, UserType.WORKER.toString()));
        }
        return users;
    }

    public User findFirstByEmail(String findEmail){
        for(User user : prepareMockDataUserManager()){
            if(user.getEmail().equals(findEmail))
                return user;
        }
        return null;
    }

    public User findFirstByUid(String findUid){
        for(User user : prepareMockDataUserManager()){
            if(user.getUid().equals(findUid))
                return user;
        }
        return null;
    }

    public Optional<User> findById(Long findId){
        for(User user : prepareMockDataUserManager()){
            if(user.getId().equals(findId))
                return Optional.of(user);
        }
        return Optional.empty();
    }
}