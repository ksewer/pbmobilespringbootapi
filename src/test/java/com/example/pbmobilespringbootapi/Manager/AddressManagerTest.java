package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Repository.AddressRepository;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class AddressManagerTest {

    @Mock
    AddressRepository addressRepository;

    @InjectMocks
    AddressManager addressManager;

    @Before
    public void init(){
        given(addressRepository.findAll()).willReturn(prepareMockData());
        given(addressRepository.findById(2L)).willReturn(java.util.Optional.ofNullable(prepareMockData().get(1)));
    }

    @Test
    public void getAllAddress() {
        List<Address> addressList = (List) addressManager.getAllAddress();
        Assert.assertThat(addressList, Matchers.hasSize(3));
    }

    @Test
    public void getAddressById() {
        Address address = null;
        ErrorMessage errorMessage;
        Object addressById = addressManager.getAddressById(2L);
        if(addressById instanceof Address){
            address = (Address) addressById;
        }
        Assert.assertNotNull(address);
        Assert.assertEquals(address.getId(),2L, 0);

        address = null;
        addressById = addressManager.getAddressById(5L);
        if(addressById instanceof Address){
            address = (Address) addressById;
        }

        Assert.assertNull(address);
    }

    public List<Address> prepareMockData(){
        List<Address> addressList = new ArrayList<>();
        addressList.add(new Address(1L, "Polska1", "Małopolskie1", "Kraków1", "Nieznana1", "1", "1", "11-111"));
        addressList.add(new Address(2L, "Polska2", "Małopolskie2", "Kraków2", "Nieznana2", "2", "2", "22-222"));
        addressList.add(new Address(3L, "Polska3", "Małopolskie3", "Kraków3", "Nieznana3", "3", "3", "33-333"));
        return addressList;
    }
}