package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Admin;
import com.example.pbmobilespringbootapi.DAO.Repository.AdminListRepository;
import com.example.pbmobilespringbootapi.Enums.UserType;
import com.example.pbmobilespringbootapi.ErrorMessage;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class AdminListManagerTest {

    @Mock
    AdminListRepository adminListRepository;

    @InjectMocks
    AdminListManager adminListManager;

    @Before
    public void init() {
        given(adminListRepository.findAll()).willReturn(prepareMockData());
        given(adminListRepository.findFirstByEmail("romek12ziomek@gmail.com")).willReturn(prepareMockData().get(0));
    }

    @Test
    public void checkEmail() {
        Admin admin = prepareMockData().get(0);
        Object obj = adminListManager.checkEmail(admin);
        Admin adminFinded = null;
        if (obj instanceof Admin)
            adminFinded = (Admin) obj;
        Assert.assertNotNull(adminFinded);
        Assert.assertEquals(adminFinded.getId(), 1L, 0);

        admin = new Admin(3L, "romek12koles@gmail.com", UserType.OWNER.toString());
        obj = adminListManager.checkEmail(admin);
        ErrorMessage errorMessage = null;
        if (obj instanceof ErrorMessage)
            errorMessage = (ErrorMessage) obj;
        Assert.assertNotNull(errorMessage);
        Assert.assertEquals(errorMessage.getMessages().get(0).getErrorNumber(), ErrorMessage.ErrorNumberConst.NOTFOUNDED.toString());

        obj = adminListManager.checkEmail(null);
        errorMessage = null;
        if (obj instanceof ErrorMessage)
            errorMessage = (ErrorMessage) obj;
        Assert.assertNotNull(errorMessage);
        Assert.assertEquals(errorMessage.getMessages().get(0).getErrorNumber(), ErrorMessage.ErrorNumberConst.NULLARGUMENT.toString());

    }

    @Test
    public void getAll() {
        List<Admin> adminList = (List) adminListManager.getAll();
        Assert.assertThat(adminList, Matchers.hasSize(2));
    }

    public List<Admin> prepareMockData() {
        List<Admin> adminList = new ArrayList<>();
        adminList.add(new Admin(1L, "romek12ziomek@gmail.com", UserType.OWNER.toString()));
        adminList.add(new Admin(2L, "szajna.marcin12@gmail.com", UserType.WORKER.toString()));
        return adminList;
    }
}