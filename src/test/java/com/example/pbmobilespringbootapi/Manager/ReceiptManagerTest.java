package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.PositionOnReceipt;
import com.example.pbmobilespringbootapi.DAO.Entity.Receipt;
import com.example.pbmobilespringbootapi.DAO.Repository.ReceiptRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ReceiptManagerTest {

    @Mock
    ReceiptRepository receiptRepository;

    @InjectMocks
    ReceiptManager receiptManager;

    @Before
    public void init(){
        given(receiptRepository.findAll()).willReturn(prepareMockData());
    }

    @Test
    public void getAllReceipt() {
        List<Receipt> receipts = (ArrayList) receiptManager.getAllReceipt();
        Assert.assertThat(receipts, Matchers.hasSize(2));
    }

    public List<Receipt> prepareMockData(){
        List<Receipt> receipts = new ArrayList<>();
        Receipt receipt = new Receipt("karta", "123456789", null, LocalDateTime.now(), 1L);
        PositionOnReceipt position1 = new PositionOnReceipt(1L, 20f);
        receipt.setPositionOnReceipt(position1);
        receipts.add(receipt);
        receipt = new Receipt("gotowka", "123456789", null, LocalDateTime.now(), 2L);
        position1 = new PositionOnReceipt(1L, 30f);
        receipt.setPositionOnReceipt(position1);
        receipts.add(receipt);

        return receipts;
    }
}