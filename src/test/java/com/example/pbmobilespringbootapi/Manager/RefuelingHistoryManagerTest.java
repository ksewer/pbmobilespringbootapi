package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Entity.RefuelingHistory;
import com.example.pbmobilespringbootapi.DAO.Entity.User;
import com.example.pbmobilespringbootapi.DAO.Repository.RefuelingHistoryRepository;
import com.example.pbmobilespringbootapi.DAO.Repository.UserRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class RefuelingHistoryManagerTest {

    @Mock
    RefuelingHistoryRepository refuelingHistoryRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    UserManager userManager;

    @InjectMocks
    RefuelingHistoryManager refuelingHistoryManager;

    @Before
    public void init(){
        User user = new User();
        user.setId(1L);
        given(refuelingHistoryRepository.findAll()).willReturn(prepareMockData());
        given(userManager.getUserById(1L)).willReturn(user);
    }

    @Test
    public void getAllByUser() {
        Iterable<RefuelingHistory> all = refuelingHistoryManager.getAllByUser(1L);
        Assert.assertThat((List<RefuelingHistory>)all, Matchers.hasSize(0));
    }

    @Test
    public void getAll() {
        Iterable<RefuelingHistory> all = refuelingHistoryManager.getAll();
        Assert.assertThat((List<RefuelingHistory>)all, Matchers.hasSize(5));
    }

    public List<RefuelingHistory> prepareMockData(){
        List<RefuelingHistory> refuelingHistoryList = new ArrayList<>();

        User userWithId1 = new User();
        userWithId1.setId(1L);

        User userWithId2 = new User();
        userWithId2.setId(2L);

        refuelingHistoryList.add(new RefuelingHistory(25f, LocalDateTime.now(), 35, userWithId1, new Product()));
        refuelingHistoryList.add(new RefuelingHistory(30f, LocalDateTime.now(), 45, userWithId1, new Product()));
        refuelingHistoryList.add(new RefuelingHistory(30f, LocalDateTime.now(), 45, userWithId1, new Product()));
        refuelingHistoryList.add(new RefuelingHistory(35f, LocalDateTime.now(), 55, userWithId2, new Product()));
        refuelingHistoryList.add(new RefuelingHistory(35f, LocalDateTime.now(), 55, userWithId2, new Product()));

        return refuelingHistoryList;
    }

    public Iterable<RefuelingHistory> findAllByUser(Long idUser){
        List<RefuelingHistory> refuelingHistoryList = new ArrayList<>();
        for(RefuelingHistory refuelingHistory : prepareMockData()){
            if(refuelingHistory.getUser().getId() == idUser)
                refuelingHistoryList.add(refuelingHistory);
        }
        return refuelingHistoryList;
    }
}