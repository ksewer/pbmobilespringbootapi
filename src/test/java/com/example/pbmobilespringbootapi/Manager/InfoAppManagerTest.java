package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Entity.InfoApp;
import com.example.pbmobilespringbootapi.DAO.Repository.InfoAppRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class InfoAppManagerTest {

    @Mock
    InfoAppRepository infoAppRepository;

    @InjectMocks
    InfoAppManager infoAppManager;

    @Before
    public void init(){
        given(infoAppRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(prepareMockData().get(0)));
    }

    @Test
    public void getInfo() {
        Object obj = infoAppManager.getInfo();
        if(obj instanceof InfoApp)
            Assert.assertNotNull(obj);
        else
            Assert.assertNull(obj);
    }

    public List<InfoApp> prepareMockData(){
        List<InfoApp> infoAppList = new ArrayList<>();
        Address address = new Address();
        Company company = new Company();
        address.setCountry("Polska");
        address.setVoivodeship("Małopolska");
        address.setCity("Kraków");
        address.setZipCode("31-864");
        address.setStreet("Jana Pawła II");
        address.setBuildingNumber("37");

        company.setAddress(new Address());
        company.getAddress().setCountry("Polska");
        company.getAddress().setVoivodeship("Małopolska");
        company.getAddress().setCity("Kraków");
        company.getAddress().setZipCode("31-864");
        company.getAddress().setStreet("Jana Pawła II");
        company.getAddress().setBuildingNumber("37");
        company.setNip("9856473812");
        company.setREGON("75698775");
        company.setName("PB");
        infoAppList.add(new InfoApp(address, company, "(070) 012-34-56", "Jan Kowalski"));
        return infoAppList;
    }

}