package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Address;
import com.example.pbmobilespringbootapi.DAO.Entity.Company;
import com.example.pbmobilespringbootapi.DAO.Repository.CompanyRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CompanyManagerTest {

    @Mock
    CompanyRepository companyRepository;

    @InjectMocks
    CompanyManager companyManager;

    @Before
    public void init() {
        given(companyRepository.findAll()).willReturn(prepareMockData());
        given(companyRepository.findByNip("1234569871")).willReturn(findCompanyByNip("1234569871"));
        given(companyRepository.findFirstByREGON("654654452")).willReturn(findCompanyByRegon("654654452"));
        given(companyRepository.findById(3L)).willReturn(java.util.Optional.ofNullable(prepareMockData().get(2)));
    }

    @Test
    public void getAllCompany() {
        List<Company> companyList = (List) companyManager.getAllCompany();
        Assert.assertThat(companyList, Matchers.hasSize(3));
    }

    @Test
    public void getCompanyByNip() {
        Company company = companyManager.getCompanyByNip("1234569871");
        Assert.assertEquals(company.getId(), 1L, 0);

        company = companyManager.getCompanyByNip("3464365463456546456");
        Assert.assertNull(company);
    }

    @Test
    public void getCompanyByRegon() {
        Object obj = companyManager.getCompanyByRegon("654654452");
        Company company = null;
        if (obj instanceof Company)
            company = (Company) obj;
        Assert.assertNotNull(company);
        Assert.assertEquals(company.getId(), 2L, 0);

        obj = companyManager.getCompanyByRegon("643523452345");
        company = null;
        if (obj instanceof Company)
            company = (Company) obj;
        Assert.assertNull(company);
    }

    @Test
    public void getCompanyById() {
        Company company = companyManager.getCompanyById(3L);
        Assert.assertEquals(company.getId(), 3L, 0);

        company = companyManager.getCompanyById(65L);
        Assert.assertNull(company);
    }

    public static List<Company> prepareMockData() {
        List<Company> companyList = new ArrayList<>();
        Company company = new Company(1L, "Firma1Name1", "1234569871", "654654451", null);
        company.setAddress(new Address(1L, "Polska1", "Małopolskie1", "Kraków1", "Nieznana1", "1", "1", "11-111"));
        companyList.add(company);
        company = new Company(2L, "Firma2Name2", "1234569872", "654654452", null);
        company.setAddress(new Address(2L, "Polska2", "Małopolskie2", "Kraków2", "Nieznana2", "2", "2", "22-222"));
        companyList.add(company);
        company = new Company(3L, "Firma3Name3", "1234569873", "654654453", null);
        company.setAddress(new Address(3L, "Polska3", "Małopolskie3", "Kraków3", "Nieznana3", "3", "3", "33-333"));
        companyList.add(company);
        return companyList;
    }

    public Company findCompanyByNip(String nip) {
        for (Company company : prepareMockData()) {
            if (company.getNip().equals(nip))
                return company;
        }
        return null;
    }

    public Company findCompanyByRegon(String regon) {
        for (Company company : prepareMockData()) {
            if (company.getREGON().equals(regon))
                return company;
        }
        return null;
    }
}