package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.MagazinePosition;
import com.example.pbmobilespringbootapi.DAO.Entity.Product;
import com.example.pbmobilespringbootapi.DAO.Repository.MagazinePositionRepository;
import jdk.nashorn.internal.ir.ObjectNode;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class MagazinePositionManagerTest {

    @Mock
    MagazinePositionRepository magazinePositionRepository;

    @Mock
    ProductManager productManager;

    @InjectMocks
    MagazinePositionManager magazinePositionManager;

    @Before
    public void init() {
        Product productWithId1 = ProductManagerTest.prepareMockData().get(0);
        given(magazinePositionRepository.findAll()).willReturn(prepareMockData());
        given(productManager.getProductById(1L)).willReturn(productWithId1);
        given(magazinePositionRepository.findFirstByProduct(productWithId1)).willReturn(findFirstByProduct(productWithId1));
    }

    @Test
    public void getAll() {
        List<MagazinePosition> magazinePositions = (List) magazinePositionManager.getAll();
        Assert.assertThat(magazinePositions, Matchers.hasSize(4));
    }

    @Test
    public void getQuantityByProductID() {
        Object obj = magazinePositionManager.getQuantityByProductID(1L);
        MagazinePosition magazinePosition = null;
        if (obj instanceof MagazinePosition)
            magazinePosition = (MagazinePosition) obj;
        Assert.assertNotNull(magazinePosition);
        Assert.assertEquals(magazinePosition.getQuantity(), 1000f, 0);
    }

    @Test
    public void getMagazinePetrolStatus() {
        Assert.assertNotNull(magazinePositionManager.getMagazinePetrolStatus());
    }

    public List<MagazinePosition> prepareMockData() {
        List<MagazinePosition> magazinePositions = new ArrayList<>();
        magazinePositions.add(new MagazinePosition(ProductManagerTest.prepareMockData().get(0), 1000f));
        magazinePositions.add(new MagazinePosition(ProductManagerTest.prepareMockData().get(1), 2000f));
        magazinePositions.add(new MagazinePosition(ProductManagerTest.prepareMockData().get(2), 3000f));
        magazinePositions.add(new MagazinePosition(ProductManagerTest.prepareMockData().get(3), 4000f));
        return magazinePositions;
    }

    public MagazinePosition findFirstByProduct(Product product) {
        for (MagazinePosition mPosition : prepareMockData()) {
            if (mPosition.getProduct().getId().equals(product.getId()))
                return mPosition;
        }
        return null;
    }
}