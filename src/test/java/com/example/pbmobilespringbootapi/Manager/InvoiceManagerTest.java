package com.example.pbmobilespringbootapi.Manager;

import com.example.pbmobilespringbootapi.DAO.Entity.Invoice;
import com.example.pbmobilespringbootapi.DAO.Entity.PositionOnReceipt;
import com.example.pbmobilespringbootapi.DAO.Repository.InvoiceRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceManagerTest {

    @Mock
    InvoiceRepository invoiceRepository;

    @InjectMocks
    InvoiceManager invoiceManager;

    @Before
    public void init(){
        given(invoiceRepository.findAll()).willReturn(prepareMockData());
    }

    @Test
    public void getAllInvoice() {
        List<Invoice> invoiceList = (List) invoiceManager.getAllInvoice();
        Assert.assertThat(invoiceList, Matchers.hasSize(3));
    }

    public List<Invoice> prepareMockData(){
        List<Invoice> invoiceList = new ArrayList<>();
        Invoice invoice = new Invoice("przelew", null, CompanyManagerTest.prepareMockData().get(0), LocalDateTime.now(), 21, 1L);
        PositionOnReceipt position1 = new PositionOnReceipt(4L, 14.5f);
        invoice.setPositionOnReceipt(position1);
        invoiceList.add(invoice);

        invoice = new Invoice("karta", null, CompanyManagerTest.prepareMockData().get(1), LocalDateTime.now(), 7, 2L);
        position1 = new PositionOnReceipt(4L, 24.5f);
        invoice.setPositionOnReceipt(position1);
        invoiceList.add(invoice);

        invoice = new Invoice("gotowka", null, CompanyManagerTest.prepareMockData().get(2), LocalDateTime.now(), 14, 1L);
        position1 = new PositionOnReceipt(4L, 34.5f);
        invoice.setPositionOnReceipt(position1);
        invoiceList.add(invoice);
        return invoiceList;
    }
}